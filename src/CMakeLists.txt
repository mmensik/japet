cmake_minimum_required (VERSION 2.6) 

project (Japet)

set (PETSC_DIR /Users/martinmensik/Workplace/petsc3.1)
set (PETSC_ARCH mac-cxx)

set(Boost_USE_STATIC_LIBS   ON)
set(Boost_USE_MULTITHREADED ON)

SET(CMAKE_C_COMPILER mpicc)
SET(CMAKE_CXX_COMPILER mpicxx)

find_package(Boost 1.45.0 COMPONENTS thread)

include (${PETSC_DIR}/${PETSC_ARCH}/conf/PETScConfig.cmake)
include_directories (${PETSC_DIR}/include ${PETSC_DIR}/${PETSC_ARCH}/include ${PROJECT_SOURCE_DIR} ${Boost_INCLUDE_DIR}) 

link_directories (${PETSC_DIR}/${PETSC_ARCH}/lib)

set (LIBRARIES petsc 
		     ${PETSC_PACKAGE_LIBS} 
	             ${Boost_FILESYSTEM_LIBRARY}
                     ${Boost_SYSTEM_LIBRARY}
                     ${Boost_UNIT_TEST_FRAMEWORK_LIBRARY})

add_subdirectory (Utils)
add_subdirectory (Solvers)
add_subdirectory (Structures)
add_subdirectory (Fem)
add_subdirectory (Feti)

add_executable (TestBuild "${PROJECT_SOURCE_DIR}/test/testBuild.cpp")
target_link_libraries (TestBuild Utils ${LIBRARIES})

add_executable (TestHTFETI "${PROJECT_SOURCE_DIR}/test/testHFeti.cpp")
target_link_libraries (TestHTFETI Utils Fem Solvers Feti ${LIBRARIES})

add_executable (TestFETI "${PROJECT_SOURCE_DIR}/test/testFeti.cpp")
target_link_libraries (TestFETI Utils Fem Solvers Feti ${LIBRARIES})

add_executable (TestBoost "${PROJECT_SOURCE_DIR}/test/testBoost.cpp")
target_link_libraries (TestBoost ${LIBRARIES})
