static char help[] = "My first own testing utility for PETSc\n\n";

#include <iostream>
#include "petsc.h"

#include "Utils/timer.h"

int main(int argc, char *argv[]) {

	PetscInitialize(&argc, &argv, (char *) 0, help);

	MyTimer myTime;

	std::cout << myTime.getTotalTime() << std::endl;

	PetscFinalize();
	return 0;

}
