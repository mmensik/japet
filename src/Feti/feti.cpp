#include "feti.h"

//
//
//
// ******************************************************************************
//
//

void GenerateJumpOperator(Mesh *mesh, Mat &B, Vec &lmb) {
	PetscInt rank;
	MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

	MatCreateMPIAIJ(PETSC_COMM_WORLD, PETSC_DECIDE, mesh->vetrices.size(), mesh->nPairs, PETSC_DECIDE, 2, PETSC_NULL, 2, PETSC_NULL, &B);

	if (!rank) {
		for (int i = 0; i < mesh->nPairs; i++) {
			MatSetValue(B, i, mesh->pointPairing[i * 2], 1, INSERT_VALUES);
			MatSetValue(B, i, mesh->pointPairing[i * 2 + 1], -1, INSERT_VALUES);
		}
	}

	MatAssemblyBegin(B, MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(B, MAT_FINAL_ASSEMBLY);

	VecCreate(PETSC_COMM_WORLD, &lmb);
	VecSetSizes(lmb, PETSC_DECIDE, mesh->nPairs);
	VecSetFromOptions(lmb);
	VecSet(lmb, 0);
}

void GenerateTotalJumpOperator(Mesh *mesh, int d, Mat &B, Mat &BT, Vec &lmb,
		PDCommManager* commManager) {

	int dSum;

	if (commManager->isPrimal()) {

		//
		// Compute overall primal size
		//
		PetscInt localNodeCount, globalNodeCount;
		localNodeCount = mesh->vetrices.size();
		MPI_Reduce(&localNodeCount, &globalNodeCount, 1, MPI_INT, MPI_SUM, 0, commManager->getPrimal());

		//
		//Put all dirchlet nodes to root
		//
		int dSize;
		std::set<PetscInt> indDirchlet;
		for (std::set<PetscInt>::iterator i = mesh->borderEdges.begin();
				i != mesh->borderEdges.end(); i++) {

			for (int j = 0; j < 2; j++) {
				indDirchlet.insert(mesh->edges[*i]->vetrices[j]);
			}
		}
		dSize = indDirchlet.size();

		PetscInt locDirch[dSize];
		int counter = 0;
		for (std::set<PetscInt>::iterator i = indDirchlet.begin();
				i != indDirchlet.end(); i++) {
			locDirch[counter++] = *i;
		}

		int dNodeCounts[commManager->getPrimalSize()];

		MPI_Allgather(&dSize, 1, MPI_INT, dNodeCounts, 1, MPI_INT, commManager->getPrimal());

		dSum = 0;
		for (int i = 0; i < commManager->getPrimalSize(); i++)
			dSum += dNodeCounts[i];

		//
		// Create BT matrix on PRIMAL
		//
		MatCreateMPIAIJ(commManager->getPrimal(), mesh->vetrices.size() * d, PETSC_DECIDE, PETSC_DECIDE, (mesh->nPairs
				+ dSum) * d, 2, PETSC_NULL, 2, PETSC_NULL, &BT);
		MatCreateMPIAIJ(commManager->getPrimal(), PETSC_DECIDE, mesh->vetrices.size()
				* d, (mesh->nPairs + dSum) * d, PETSC_DECIDE, 2, PETSC_NULL, 2, PETSC_NULL, &B);

		if (commManager->isPrimalRoot()) {
			//
			// TODO I suppose here, that pRank is in root in dual too!
			//

			PetscInt globDirch[dSum];
			int displac[commManager->getPrimalSize()];
			displac[0] = 0;
			for (int i = 1; i < commManager->getPrimalSize(); i++)
				displac[i] = displac[i - 1] + dNodeCounts[i - 1];

			MPI_Gatherv(locDirch, dSize, MPI_INT, globDirch, dNodeCounts, displac, MPI_INT, 0, commManager->getPrimal());

			int sIndex = 0;
			for (int i = 0; i < dSum; i++) {
				for (int j = 0; j < d; j++) {
					MatSetValue(B, sIndex, globDirch[i] * d + j, 1, INSERT_VALUES);
					MatSetValue(BT, globDirch[i] * d + j, sIndex, 1, INSERT_VALUES);
					sIndex++;
				}
			}

			PetscReal boundVal = 1.0 / sqrt(2.0); // Value to keep the B ortonormal

			std::set<PetscInt> cornerInd;

			for (unsigned int i = 0; i < mesh->corners.size(); i++) {
				for (int j = 0; j < mesh->corners[i]->cornerSize; j++) {
					cornerInd.insert(mesh->corners[i]->vetrices[j]);
				}
			}

			PetscInt rowCounter = dSum;
			for (int i = 0; i < mesh->nPairs; i++) {
				if (cornerInd.count(mesh->pointPairing[i * 2]) == 0
						&& cornerInd.count(mesh->pointPairing[i * 2 + 1]) == 0) {
					for (int j = 0; j < d; j++) {

						MatSetValue(B, rowCounter * d + j, mesh->pointPairing[i * 2] * d
								+ j, boundVal, INSERT_VALUES);
						MatSetValue(B, rowCounter * d + j, mesh->pointPairing[i * 2 + 1] * d
								+ j, -boundVal, INSERT_VALUES);

						MatSetValue(BT, mesh->pointPairing[i * 2] * d + j, rowCounter * d
								+ j, boundVal, INSERT_VALUES);
						MatSetValue(BT, mesh->pointPairing[i * 2 + 1] * d + j, rowCounter
								* d + j, -boundVal, INSERT_VALUES);

					}
					rowCounter++;
				}
			}

			for (unsigned int i = 0; i < mesh->corners.size(); i++) {

				PetscInt *vetrices = mesh->corners[i]->vetrices;
				PetscInt cornerSize = mesh->corners[i]->cornerSize;

				for (int j = 0; j < cornerSize - 1; j++) {

					PetscReal norm = sqrt((PetscReal) (cornerSize - j - 1)
							* (cornerSize - j - 1) + (cornerSize - j - 1));

					for (int dim = 0; dim < d; dim++) {
						MatSetValue(B, rowCounter * d + dim, vetrices[j] * d + dim, -(cornerSize
								- j - 1) / norm, INSERT_VALUES);
						MatSetValue(BT, vetrices[j] * d + dim, rowCounter * d + dim, -(cornerSize
								- j - 1) / norm, INSERT_VALUES);

						for (int k = j + 1; k < cornerSize; k++) {
							MatSetValue(B, rowCounter * d + dim, vetrices[k] * d + dim, 1
									/ norm, INSERT_VALUES);
							MatSetValue(BT, vetrices[k] * d + dim, rowCounter * d + dim, 1
									/ norm, INSERT_VALUES);
						}
					}

					rowCounter++;
				}
			}

		} else {
			//
			//Primal nonroots
			//
			MPI_Gatherv(locDirch, dSize, MPI_INT, NULL, 0, NULL, MPI_INT, 0, commManager->getPrimal());
		}
		{

			MatAssemblyBegin(B, MAT_FINAL_ASSEMBLY);
			MatAssemblyBegin(BT, MAT_FINAL_ASSEMBLY);
			MatAssemblyEnd(B, MAT_FINAL_ASSEMBLY);
			MatAssemblyEnd(BT, MAT_FINAL_ASSEMBLY);
		}
	}

	if (commManager->isDual()) {
		int BSize;

		if (commManager->isDualRoot()) {
			BSize = (mesh->nPairs + dSum) * d;
		}

		MPI_Bcast(&BSize, 1, MPI_INT, 0, commManager->getDual());

		VecCreateMPI(commManager->getDual(), PETSC_DECIDE, BSize, &lmb);
		VecSet(lmb, 0);
	}

}

void GenerateClusterJumpOperator(Mesh *mesh, SubdomainCluster *cluster,
		Mat &BGlob, Mat &BTGlob, Vec &lmbGlob, Mat &BCluster, Mat &BTCluster,
		Vec &lmbCluster, MPI_Comm comm) {

	PetscInt rank, subRank, size;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_rank(cluster->clusterComm, &subRank);

	MPI_Comm_size(comm, &size);

	int d = 2;
	//
	//Put all dirchlet nodes to root
	//

	int dSize;
	std::set<PetscInt> indDirchlet;
	for (std::set<PetscInt>::iterator i = mesh->borderEdges.begin();
			i != mesh->borderEdges.end(); i++) {

		for (int j = 0; j < 2; j++) {
			indDirchlet.insert(mesh->edges[*i]->vetrices[j]);
		}

	}
	dSize = indDirchlet.size();

	PetscInt locDirch[dSize];
	int counter = 0;
	for (std::set<PetscInt>::iterator i = indDirchlet.begin();
			i != indDirchlet.end(); i++) {
		locDirch[counter++] = *i;
	}
	int dNodeCounts[size];
	MPI_Allgather(&dSize, 1, MPI_INT, dNodeCounts, 1, MPI_INT, comm);

	int dSum = 0;
	for (int i = 0; i < size; i++)
		dSum += dNodeCounts[i];

	int globalPairsCount = cluster->globalPairing.size() / 2;
	MPI_Bcast(&globalPairsCount, 1, MPI_INT, 0, comm);

	MatCreateMPIAIJ(comm, PETSC_DECIDE, mesh->vetrices.size() * d, (globalPairsCount
			+ dSum) * d, PETSC_DECIDE, 4, PETSC_NULL, 4, PETSC_NULL, &BGlob);
	MatCreateMPIAIJ(comm, mesh->vetrices.size() * d, PETSC_DECIDE, PETSC_DECIDE, (globalPairsCount
			+ dSum) * d, 4, PETSC_NULL, 4, PETSC_NULL, &BTGlob);

	if (!rank) {

		PetscInt globDirch[dSum];
		int displac[size];
		displac[0] = 0;
		for (int i = 1; i < size; i++)
			displac[i] = displac[i - 1] + dNodeCounts[i - 1];

		MPI_Gatherv(locDirch, dSize, MPI_INT, globDirch, dNodeCounts, displac, MPI_INT, 0, comm);

		int sIndex = 0;
		for (int i = 0; i < dSum; i++) {
			for (int j = 0; j < d; j++) {
				MatSetValue(BGlob, sIndex, globDirch[i] * d + j, 1, INSERT_VALUES);
				MatSetValue(BTGlob, globDirch[i] * d + j, sIndex, 1, INSERT_VALUES);
				sIndex++;
			}
		}

		typedef std::map<PetscInt, std::set<PetscInt> > PGroupMap;
		PGroupMap equalGroups;

		for (std::vector<PetscInt>::iterator i = cluster->globalPairing.begin();
				i != cluster->globalPairing.end(); i = i + 2) {

			std::set<PetscInt> newSet;

			PetscInt pair[] = { *i, *(i + 1) };

			for (int j = 0; j < 2; j++) {
				newSet.insert(pair[j]);

				for (std::set<PetscInt>::iterator k = equalGroups[pair[j]].begin();
						k != equalGroups[pair[j]].end(); k++) {
					newSet.insert(*k);
				}
			}

			for (std::set<PetscInt>::iterator j = newSet.begin(); j != newSet.end();
					j++) {
				for (std::set<PetscInt>::iterator k = newSet.begin(); k != newSet.end();
						k++) {
					equalGroups[*j].insert(*k);
				}
			}
		}

		// for (PGroupMap::iterator i = equalGroups.begin(); i != equalGroups.end(); i++) {
		// PetscPrintf(PETSC_COMM_SELF, "[%d] - ", i->first);
		// for (std::set<PetscInt>::iterator j = i->second.begin(); j
		// != i->second.end(); j++) {
		// PetscPrintf(PETSC_COMM_SELF, " %d ", *j);
		// }
		// PetscPrintf(PETSC_COMM_SELF, "\n");
		// }

		PetscInt rowCounter = dSum;

		while (equalGroups.size() > 0) {

			std::set<PetscInt> p = equalGroups.begin()->second;

			PetscInt cornerSize = p.size();
			PetscInt *vetrices = new PetscInt[cornerSize];

			int cc = 0;
			for (std::set<PetscInt>::iterator j = p.begin(); j != p.end(); j++) {
				vetrices[cc++] = *j;
				equalGroups.erase(*j);
			}

			for (int j = 0; j < cornerSize - 1; j++) {

				PetscReal norm = sqrt((PetscReal) (cornerSize - j - 1)
						* (cornerSize - j - 1) + (cornerSize - j - 1));

				for (int dim = 0; dim < d; dim++) {
					MatSetValue(BGlob, rowCounter * d + dim, vetrices[j] * d + dim, -(cornerSize
							- j - 1) / norm, INSERT_VALUES);
					MatSetValue(BTGlob, vetrices[j] * d + dim, rowCounter * d + dim, -(cornerSize
							- j - 1) / norm, INSERT_VALUES);

					for (int k = j + 1; k < cornerSize; k++) {
						MatSetValue(BGlob, rowCounter * d + dim, vetrices[k] * d + dim, 1
								/ norm, INSERT_VALUES);
						MatSetValue(BTGlob, vetrices[k] * d + dim, rowCounter * d + dim, 1
								/ norm, INSERT_VALUES);
					}
				}
				rowCounter++;
			}
			delete[] vetrices;
		}
	} else {
		MPI_Gatherv(locDirch, dSize, MPI_INT, NULL, 0, NULL, MPI_INT, 0, comm);
	}

	{

		MatAssemblyBegin(BGlob, MAT_FINAL_ASSEMBLY);
		MatAssemblyEnd(BGlob, MAT_FINAL_ASSEMBLY);

		MatAssemblyBegin(BTGlob, MAT_FINAL_ASSEMBLY);
		MatAssemblyEnd(BTGlob, MAT_FINAL_ASSEMBLY);
	}

	VecCreateMPI(comm, PETSC_DECIDE, (globalPairsCount + dSum) * d, &lmbGlob);
	VecSet(lmbGlob, 0);

	int clusterPairCount = cluster->localPairing.size() / 2;
	MPI_Bcast(&clusterPairCount, 1, MPI_INT, 0, cluster->clusterComm);

	MatCreateMPIAIJ(cluster->clusterComm, PETSC_DECIDE, mesh->vetrices.size() * d, clusterPairCount
			* d, PETSC_DECIDE, 4, PETSC_NULL, 4, PETSC_NULL, &BCluster);
	MatCreateMPIAIJ(cluster->clusterComm, mesh->vetrices.size() * d, PETSC_DECIDE, PETSC_DECIDE, clusterPairCount
			* d, 4, PETSC_NULL, 4, PETSC_NULL, &BTCluster);

	VecCreateMPI(cluster->clusterComm, PETSC_DECIDE, clusterPairCount * d, &lmbCluster);

	if (!subRank) {

		typedef std::map<PetscInt, std::set<PetscInt> > PGroupMap;
		PGroupMap equalGroups;

		for (std::vector<PetscInt>::iterator i = cluster->localPairing.begin();
				i != cluster->localPairing.end(); i = i + 2) {

			std::set<PetscInt> newSet;

			PetscInt pair[] = { *i, *(i + 1) };

			for (int j = 0; j < 2; j++) {
				newSet.insert(pair[j]);

				for (std::set<PetscInt>::iterator k = equalGroups[pair[j]].begin();
						k != equalGroups[pair[j]].end(); k++) {
					newSet.insert(*k);
				}
			}

			for (std::set<PetscInt>::iterator j = newSet.begin(); j != newSet.end();
					j++) {
				for (std::set<PetscInt>::iterator k = newSet.begin(); k != newSet.end();
						k++) {
					equalGroups[*j].insert(*k);
				}
			}
		}

		PetscInt rowCounter = 0;
		while (equalGroups.size() > 0) {

			std::set<PetscInt> p = equalGroups.begin()->second;

			PetscInt cornerSize = p.size();
			PetscInt *vetrices = new PetscInt[cornerSize];

			int cc = 0;
			for (std::set<PetscInt>::iterator j = p.begin(); j != p.end(); j++) {

				PetscInt globalIndex = *j;
				PetscInt clusterIndex = globalIndex
						+ cluster->startIndexesDiff[mesh->getNodeDomain(globalIndex)];
				vetrices[cc++] = clusterIndex;

				equalGroups.erase(*j);
			}

			for (int j = 0; j < cornerSize - 1; j++) {

				PetscReal norm = sqrt((PetscReal) (cornerSize - j - 1)
						* (cornerSize - j - 1) + (cornerSize - j - 1));

				for (int dim = 0; dim < d; dim++) {

					MatSetValue(BCluster, rowCounter * d + dim, vetrices[j] * d + dim, -(cornerSize
							- j - 1) / norm, INSERT_VALUES);
					MatSetValue(BTCluster, vetrices[j] * d + dim, rowCounter * d + dim, -(cornerSize
							- j - 1) / norm, INSERT_VALUES);

					for (int k = j + 1; k < cornerSize; k++) {
						MatSetValue(BCluster, rowCounter * d + dim, vetrices[k] * d + dim, 1
								/ norm, INSERT_VALUES);
						MatSetValue(BTCluster, vetrices[k] * d + dim, rowCounter * d + dim, 1
								/ norm, INSERT_VALUES);
					}
				}

				rowCounter++;
			}

			delete[] vetrices;
		}
	}

	{

		MatAssemblyBegin(BCluster, MAT_FINAL_ASSEMBLY);
		MatAssemblyEnd(BCluster, MAT_FINAL_ASSEMBLY);

		MatAssemblyBegin(BTCluster, MAT_FINAL_ASSEMBLY);
		MatAssemblyEnd(BTCluster, MAT_FINAL_ASSEMBLY);
	}

	VecSet(lmbCluster, 0);

}

void getLocalJumpPart(Mat B, Mat *Bloc) {

	PetscInt m, n, rows;
	MatGetOwnershipRangeColumn(B, &m, &n);
	MatGetSize(B, &rows, PETSC_NULL);
	PetscInt size = n - m;

	IS ISlocal, ISlocalRows;
	ISCreateStride(PETSC_COMM_SELF, size, m, 1, &ISlocal);
	ISCreateStride(PETSC_COMM_SELF, rows, 0, 1, &ISlocalRows);
	Mat *sm;
	MatGetSubMatrices(B, 1, &ISlocalRows, &ISlocal, MAT_INITIAL_MATRIX, &sm);
	*Bloc = *sm;

}

void Generate2DLaplaceNullSpace(Mesh *mesh, bool &isSingular,
		bool &isLocalSingular, Mat *R, MPI_Comm comm) {
	PetscInt rank, size;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);

	//Zjisti, zda ma subdomena na tomto procesoru dirchletovu hranici (zda je regularni)
	PetscInt hasDirchBound = 0;
	isLocalSingular = true;

	if (mesh->borderEdges.size() > 0) {
		hasDirchBound = 1;
		isLocalSingular = false;
	}

	PetscInt nullSpaceDim;
	MPI_Allreduce(&hasDirchBound, &nullSpaceDim, 1, MPI_INT, MPI_SUM, comm);
	//Sum number of regular subdomains
	nullSpaceDim = size - nullSpaceDim; //Dimnesion of null space is number of subdomains without dirch. border
	PetscInt nsDomInd[nullSpaceDim];

	if (nullSpaceDim > 0) {
		if (!rank) { //Master gathers array of singular domain indexes and sends it to all proceses
			MPI_Status stats;
			PetscInt counter = 0;
			if (hasDirchBound == 0) {
				nsDomInd[counter++] = rank;
			}

			for (; counter < nullSpaceDim; counter++)
				MPI_Recv(nsDomInd + counter, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, comm, &stats);
		} else {
			if (hasDirchBound == 0) MPI_Send(&rank, 1, MPI_INT, 0, 0, comm);
		}
		MPI_Bcast(nsDomInd, nullSpaceDim, MPI_INT, 0, comm);

		//Creating of matrix R - null space basis
		MatCreateMPIDense(comm, mesh->vetrices.size(), PETSC_DECIDE, PETSC_DECIDE, nullSpaceDim, PETSC_NULL, R);
		for (int i = 0; i < nullSpaceDim; i++) {
			if (nsDomInd[i] == rank) {
				for (std::map<PetscInt, Point*>::iterator v = mesh->vetrices.begin();
						v != mesh->vetrices.end(); v++) {
					MatSetValue(*R, v->first, i, 1, INSERT_VALUES);
				}
			}
		}

		MatAssemblyBegin(*R, MAT_FINAL_ASSEMBLY);
		MatAssemblyEnd(*R, MAT_FINAL_ASSEMBLY);
		isSingular = true;
		PetscPrintf(comm, "Null space dimension: %d \n", nullSpaceDim);
	} else {
		isSingular = false;
	}
}

void Generate2DLaplaceTotalNullSpace(Mesh *mesh, NullSpaceInfo *nullSpace,
		MPI_Comm comm) {
	PetscInt rank, size;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);

	nullSpace->localDimension = 1;
	nullSpace->isDomainSingular = true;
	nullSpace->isSubDomainSingular = true;

	nullSpace->localBasis = new Vec[1];

	VecCreateSeq(PETSC_COMM_SELF, mesh->vetrices.size(), &(nullSpace->localBasis[0]));
	VecSet(nullSpace->localBasis[0], 1 / sqrt((double) mesh->vetrices.size()));

	Mat *R = &(nullSpace->R);
	//Creating of matrix R - null space basis
	MatCreateMPIDense(comm, mesh->vetrices.size(), PETSC_DECIDE, PETSC_DECIDE, size, PETSC_NULL, R);
	for (int i = 0; i < size; i++) {
		if (i == rank) {
			for (std::map<PetscInt, Point*>::iterator v = mesh->vetrices.begin();
					v != mesh->vetrices.end(); v++) {
				MatSetValue(*R, v->first, i, 1 / sqrt((double) mesh->vetrices.size()), INSERT_VALUES);
			}
		}
		MatAssemblyBegin(*R, MAT_FINAL_ASSEMBLY);
		MatAssemblyEnd(*R, MAT_FINAL_ASSEMBLY);
	}

}

void Generate2DElasticityNullSpace(Mesh *mesh, NullSpaceInfo *nullSpace,
		MPI_Comm comm) {

	PetscInt rank, size;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);

	nullSpace->localDimension = 3;
	nullSpace->isDomainSingular = true;
	nullSpace->isSubDomainSingular = true;

	//Creating of matrix R - null space basis
	Mat *R = &(nullSpace->R);

	nullSpace->localBasis = new Vec[3];
	for (int i = 0; i < 3; i++) {
		VecCreateSeq(PETSC_COMM_SELF, mesh->vetrices.size() * 2, &(nullSpace->localBasis[i]));
	}

	for (int i = 0; i < size; i++) {
		if (i == rank) {
			for (std::map<PetscInt, Point*>::iterator v = mesh->vetrices.begin();
					v != mesh->vetrices.end(); v++) {
				VecSetValue(nullSpace->localBasis[0], (v->first - mesh->startIndexes[i])
						* 2, 1, INSERT_VALUES);
				VecSetValue(nullSpace->localBasis[1], (v->first - mesh->startIndexes[i])
						* 2 + 1, 1, INSERT_VALUES);
				VecSetValue(nullSpace->localBasis[2], (v->first - mesh->startIndexes[i])
						* 2, -v->second->y, INSERT_VALUES);
				VecSetValue(nullSpace->localBasis[2], (v->first - mesh->startIndexes[i])
						* 2 + 1, v->second->x, INSERT_VALUES);
			}
		}
	}

	//Null space ortonormalization
	PetscReal vecNorm1, vecNorm2;

	VecNorm(nullSpace->localBasis[0], NORM_2, &vecNorm1);
	VecScale(nullSpace->localBasis[0], 1 / vecNorm1);
	VecScale(nullSpace->localBasis[1], 1 / vecNorm1);

	for (int i = 0; i < 2; i++) {
		PetscReal a0;
		VecDot(nullSpace->localBasis[i], nullSpace->localBasis[2], &a0);
		VecAXPY(nullSpace->localBasis[2], -a0, nullSpace->localBasis[i]);
	}

	VecNorm(nullSpace->localBasis[2], NORM_2, &vecNorm2);
	VecScale(nullSpace->localBasis[2], 1 / vecNorm2);

	PetscInt rowIndG[mesh->vetrices.size() * 2];

	PetscInt localRSize = mesh->vetrices.size() * 2;

	for (int i = 0; i < localRSize; i++) {
		rowIndG[i] = i + mesh->startIndexes[rank] * 2;
	}

	//MatCreateMPIDense(comm, mesh->vetrices.size() * 2, PETSC_DECIDE, PETSC_DECIDE, size
	//		* 3, PETSC_NULL, R);
	MatCreateMPIAIJ(comm, mesh->vetrices.size() * 2, 3, PETSC_DECIDE, PETSC_DECIDE, 3, PETSC_NULL, 0, PETSC_NULL, R);

	for (int j = 0; j < 3; j++) {
		PetscReal *values;
		PetscInt colIndG = rank * 3 + j;
		VecGetArray(nullSpace->localBasis[j], &values);
		MatSetValues(*R, localRSize, rowIndG, 1, &colIndG, values, INSERT_VALUES);
		VecRestoreArray(nullSpace->localBasis[j], &values);
	}

	MatAssemblyBegin(*R, MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(*R, MAT_FINAL_ASSEMBLY);

	//	MatView(*R, PETSC_VIEWER_STDOUT_WORLD);

}

void genClusterNullSpace(Mesh *mesh, SubdomainCluster *cluster, Mat *R) {
	PetscInt rank, size;
	MPI_Comm comm = cluster->clusterComm;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);

	cluster->isSubDomainSingular = true;

	//Creating of matrix R - null space basis
	MatCreateMPIDense(comm, mesh->vetrices.size(), PETSC_DECIDE, PETSC_DECIDE, size, PETSC_NULL, R);
	for (int i = 0; i < size; i++) {
		if (i == rank) {
			for (std::map<PetscInt, Point*>::iterator v = mesh->vetrices.begin();
					v != mesh->vetrices.end(); v++) {
				MatSetValue(*R, v->first + cluster->indexDiff, i, 1, INSERT_VALUES);
			}
		}
	}

	MatAssemblyBegin(*R, MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(*R, MAT_FINAL_ASSEMBLY);
	cluster->isClusterSingular = true;
}

void Generate2DLaplaceClusterNullSpace(Mesh *mesh, SubdomainCluster *cluster) {
	Mat RClust, RGlob;
	PetscInt rank, size, clusterRank, clusterSize;
	MPI_Comm_rank(PETSC_COMM_WORLD, &rank);
	MPI_Comm_size(PETSC_COMM_WORLD, &size);
	MPI_Comm_rank(cluster->clusterComm, &clusterRank);
	MPI_Comm_size(cluster->clusterComm, &clusterSize);

	genClusterNullSpace(mesh, cluster, &RClust);

	//Creating of matrix R - null space basis
	MatCreateMPIDense(PETSC_COMM_WORLD, mesh->vetrices.size(), PETSC_DECIDE, PETSC_DECIDE, cluster->clusterCount, PETSC_NULL, &RGlob);
	for (int i = 0; i < cluster->clusterCount; i++) {
		if (i == cluster->clusterColor) {
			for (std::map<PetscInt, Point*>::iterator v = mesh->vetrices.begin();
					v != mesh->vetrices.end(); v++) {
				MatSetValue(RGlob, v->first, i, 1, INSERT_VALUES);
			}
		}
	}

	MatAssemblyBegin(RGlob, MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(RGlob, MAT_FINAL_ASSEMBLY);
	cluster->isDomainSingular = true;

	NullSpaceInfo *outNullSpace = new NullSpaceInfo();
	outNullSpace->R = RGlob;
	outNullSpace->isDomainSingular = true;
	outNullSpace->isSubDomainSingular = true;

	cluster->outerNullSpace = outNullSpace;
	cluster->Rin = RClust;
}

void Generate2DElasticityClusterNullSpace(Mesh *mesh, SubdomainCluster *cluster,
		MPI_Comm com_world) {

	NullSpaceInfo *nullSpace = new NullSpaceInfo();
	NullSpaceInfo *gNullSpace = new NullSpaceInfo();

	PetscInt rank, size;
	MPI_Comm_rank(com_world, &rank);
	MPI_Comm_size(com_world, &size);

	nullSpace->localDimension = 3;
	nullSpace->isDomainSingular = true;
	nullSpace->isSubDomainSingular = true;

	gNullSpace->localDimension = 3;
	gNullSpace->isDomainSingular = true;
	gNullSpace->isSubDomainSingular = true;

	//Creating of matrix R - null space basis
	Mat *R = &(nullSpace->R);
	Mat *Rglob = &(gNullSpace->R);
	Mat *systemR = &(cluster->clusterR.systemR);

	nullSpace->localBasis = new Vec[3];
	gNullSpace->localBasis = new Vec[3];

	cluster->clusterR.systemGNullSpace = new Vec[3];
	cluster->clusterR.rDim = 3;

	for (int i = 0; i < 3; i++) {
		VecCreateSeq(PETSC_COMM_SELF, mesh->vetrices.size() * 2, &(nullSpace->localBasis[i]));
		VecCreateMPI(cluster->clusterComm, mesh->vetrices.size() * 2, PETSC_DECIDE, &(gNullSpace->localBasis[i]));
		VecCreateMPI(cluster->clusterComm, 3, PETSC_DECIDE, &(cluster->clusterR.systemGNullSpace[i]));
	}

	PetscInt localRSize = mesh->vetrices.size() * 2;
	PetscInt *rowIndG = new PetscInt[localRSize];
	PetscInt *rowIndGlob = new PetscInt[localRSize];

	int cRank;
	MPI_Comm_rank(cluster->clusterComm, &cRank);

	int counter = 0;
	for (std::map<PetscInt, Point*>::iterator v = mesh->vetrices.begin();
			v != mesh->vetrices.end(); v++) {

		rowIndGlob[counter] = v->first * 2;
		rowIndG[counter++] = (v->first + cluster->indexDiff) * 2;
		rowIndGlob[counter] = v->first * 2 + 1;
		rowIndG[counter++] = (v->first + cluster->indexDiff) * 2 + 1;

		VecSetValue(nullSpace->localBasis[0], (v->first - mesh->startIndexes[rank])
				* 2, 1, INSERT_VALUES);
		VecSetValue(nullSpace->localBasis[1], (v->first - mesh->startIndexes[rank])
				* 2 + 1, 1, INSERT_VALUES);
		VecSetValue(nullSpace->localBasis[2], (v->first - mesh->startIndexes[rank])
				* 2, -v->second->y, INSERT_VALUES);
		VecSetValue(nullSpace->localBasis[2], (v->first - mesh->startIndexes[rank])
				* 2 + 1, v->second->x, INSERT_VALUES);

		VecSetValue(gNullSpace->localBasis[0], (v->first + cluster->indexDiff) * 2, 1, INSERT_VALUES);
		VecSetValue(gNullSpace->localBasis[1], (v->first + cluster->indexDiff) * 2
				+ 1, 1, INSERT_VALUES);
		VecSetValue(gNullSpace->localBasis[2], (v->first + cluster->indexDiff) * 2, -v->second->y, INSERT_VALUES);
		VecSetValue(gNullSpace->localBasis[2], (v->first + cluster->indexDiff) * 2
				+ 1, v->second->x, INSERT_VALUES);
	}

	for (int i = 0; i < 3; i++) {
		VecAssemblyBegin(gNullSpace->localBasis[i]);
		VecAssemblyEnd(gNullSpace->localBasis[i]);
	}
	//
	// Global Null space ortonormalization
	//
	PetscReal vecNorm1, vecNorm2;

	VecNorm(gNullSpace->localBasis[0], NORM_2, &vecNorm1);
	VecScale(gNullSpace->localBasis[0], 1 / vecNorm1);
	VecScale(gNullSpace->localBasis[1], 1 / vecNorm1);

	for (int i = 0; i < 2; i++) {
		PetscReal a0;
		VecDot(gNullSpace->localBasis[i], gNullSpace->localBasis[2], &a0);
		VecAXPY(gNullSpace->localBasis[2], -a0, gNullSpace->localBasis[i]);
	}

	VecNorm(gNullSpace->localBasis[2], NORM_2, &vecNorm2);
	VecScale(gNullSpace->localBasis[2], 1 / vecNorm2);

	//
	// Save to matrices
	//

	MatCreateMPIAIJ(com_world, mesh->vetrices.size() * 2, PETSC_DECIDE, PETSC_DECIDE, cluster->clusterCount
			* 3, 3, PETSC_NULL, 3, PETSC_NULL, Rglob);
	MatCreateMPIAIJ(cluster->clusterComm, mesh->vetrices.size() * 2, PETSC_DECIDE, PETSC_DECIDE, 3, 3, PETSC_NULL, 3, PETSC_NULL, systemR);

	for (int j = 0; j < 3; j++) {
		PetscReal *values;
		PetscInt colIndG = cluster->clusterColor * 3 + j;
		VecGetArray(gNullSpace->localBasis[j], &values);
		MatSetValues(*Rglob, localRSize, rowIndGlob, 1, &colIndG, values, INSERT_VALUES);
		MatSetValues(*systemR, localRSize, rowIndG, 1, &j, values, INSERT_VALUES);
		VecRestoreArray(gNullSpace->localBasis[j], &values);
	}

	delete[] rowIndGlob;

	MatAssemblyBegin(*Rglob, MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(*Rglob, MAT_FINAL_ASSEMBLY);
	MatAssemblyBegin(*systemR, MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(*systemR, MAT_FINAL_ASSEMBLY);

	//
	// Cluster Null space ortonormalization
	//

	VecNorm(nullSpace->localBasis[0], NORM_2, &vecNorm1);
	VecScale(nullSpace->localBasis[0], 1 / vecNorm1);
	VecScale(nullSpace->localBasis[1], 1 / vecNorm1);

	VecSetValue(cluster->clusterR.systemGNullSpace[0], cRank * 3, vecNorm1, INSERT_VALUES);
	VecSetValue(cluster->clusterR.systemGNullSpace[1], cRank * 3 + 1, vecNorm1, INSERT_VALUES);

	PetscReal a[2];
	for (int i = 0; i < 2; i++) {
		VecDot(nullSpace->localBasis[i], nullSpace->localBasis[2], &a[i]);
		VecAXPY(nullSpace->localBasis[2], -a[i], nullSpace->localBasis[i]);

	}

	VecNorm(nullSpace->localBasis[2], NORM_2, &vecNorm2);
	VecScale(nullSpace->localBasis[2], 1 / vecNorm2);

	VecSetValue(cluster->clusterR.systemGNullSpace[2], cRank * 3, a[0], INSERT_VALUES);
	VecSetValue(cluster->clusterR.systemGNullSpace[2], cRank * 3 + 1, a[1], INSERT_VALUES);
	VecSetValue(cluster->clusterR.systemGNullSpace[2], cRank * 3 + 2, vecNorm2, INSERT_VALUES);

	for (int i = 0; i < 3; i++) {
		VecAssemblyBegin(cluster->clusterR.systemGNullSpace[i]);
		VecAssemblyEnd(cluster->clusterR.systemGNullSpace[i]);
	}

	//System clusteR ortogonalization

	VecNorm(cluster->clusterR.systemGNullSpace[0], NORM_2, &vecNorm1);
	VecScale(cluster->clusterR.systemGNullSpace[0], 1 / vecNorm1);
	VecScale(cluster->clusterR.systemGNullSpace[1], 1 / vecNorm1);

	for (int i = 0; i < 2; i++) {
		PetscReal a0;
		VecDot(cluster->clusterR.systemGNullSpace[i], cluster->clusterR.systemGNullSpace[2], &a0);
		VecAXPY(cluster->clusterR.systemGNullSpace[2], -a0, cluster->clusterR.systemGNullSpace[i]);
	}

	VecNorm(cluster->clusterR.systemGNullSpace[2], NORM_2, &vecNorm2);
	VecScale(cluster->clusterR.systemGNullSpace[2], 1 / vecNorm2);

	//MatCreateMPIDense(comm, mesh->vetrices.size() * 2, PETSC_DECIDE, PETSC_DECIDE, size
	//		* 3, PETSC_NULL, R);
	MatCreateMPIAIJ(cluster->clusterComm, mesh->vetrices.size() * 2, 3, PETSC_DECIDE, PETSC_DECIDE, 3, PETSC_NULL, 3, PETSC_NULL, R);

	for (int j = 0; j < 3; j++) {
		PetscReal *values;
		PetscInt colIndG = cRank * 3 + j;
		VecGetArray(nullSpace->localBasis[j], &values);
		MatSetValues(*R, localRSize, rowIndG, 1, &colIndG, values, INSERT_VALUES);
		VecRestoreArray(nullSpace->localBasis[j], &values);
	}

	delete[] rowIndG;

	MatAssemblyBegin(*R, MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(*R, MAT_FINAL_ASSEMBLY);

	cluster->clusterNullSpace = nullSpace;
	cluster->outerNullSpace = gNullSpace;

}

Feti1* createFeti(Mesh *mesh, PetscReal (*f)(Point), PetscReal (*K)(Point),
		MPI_Comm comm) {
	Mat A, B;
	Vec b, lmb;
	NullSpaceInfo nullSpace;
	FEMAssemble2DLaplace(PETSC_COMM_WORLD, mesh, A, b, f, K);
	GenerateJumpOperator(mesh, B, lmb);
	Generate2DLaplaceNullSpace(mesh, nullSpace.isDomainSingular, nullSpace.isSubDomainSingular, &(nullSpace.R));

	//return new Feti1(A, b, B, lmb, &nullSpace, mesh->vetrices.size(), comm);
	return NULL;
}

JumpRectMatrix::JumpRectMatrix(PetscReal x0, PetscReal x1, PetscReal y0,
		PetscReal y1, PetscReal h, PetscInt m, PetscInt n) {

	PetscReal Hx = (x1 - x0) / (PetscReal) m;
	PetscReal Hy = (y1 - y0) / (PetscReal) n;

	xEdges = (PetscInt) ceil(Hx / h);
	yEdges = (PetscInt) ceil(Hy / h);

	nDirchlets = (yEdges + 1) * n;
	nCorners = (m - 1) * (n - 1);
	nPairs = 2 * (m - 1) * yEdges + (n - 2) * (m - 1) * (yEdges - 1)
			+ (n - 1) * xEdges + (m - 1) * (n - 1) * (xEdges - 1);

	bRows = (nDirchlets + nCorners * 3 + nPairs) * 2;

	//PetscPrintf(comm, "B has %d rows %d %d %f\n", bRows, xEdges, yEdges, h);

}

PetscInt JumpRectMatrix::getGlobalIndex(PetscInt subDom, PetscInt x,
		PetscInt y) {
	return (xEdges + 1) * (yEdges + 1) * subDom + (xEdges + 1) * y + x;
}

std::map<PetscInt, PetscReal> JumpRectMatrix::getRow(PetscInt rowNumber) {

	std::map<PetscInt, PetscReal> row;

	if (rowNumber < nDirchlets * 2) { //DIRCHLET
	//	int subDom =

	} else if (rowNumber < (nDirchlets + nCorners * 3) * 2) { //CORNER

	} else { //PAIR

	}

	return row;
}

