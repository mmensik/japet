#ifndef JAPET_FETI_HFETI_H
#define JAPET_FETI_HFETI_H

#include "Feti/feti1.h"


/**
 * @brief Hierarchical FETI implementation
 */
class HFeti: public AFeti {
	AFeti *subClusterSystem; ///< FETI1 system associated with cluste [cluster]
	SubdomainCluster *cluster; ///< Cluster info [cluster]

	Vec clustTemp, clustTempGh;
	Vec clustb;
	NullSpaceInfo *clustNullSpace;
	MatNullSpace clusterNS;

	Vec globTemp, globTempGh;

	PetscReal outerPrec;
	PetscInt inCounter;

	Vec tempInv, tempInvGh, tempInvGhB;

	Mat A;
public:
	HFeti(PDCommManager* pdMan, Mat A, Vec b, Mat BGlob, Mat BTGlob, Mat BClust,
			Mat BTClust, Vec lmbGl, Vec lmbCl, SubdomainCluster *cluster,
			PetscInt localNodeCount);

	~HFeti();
	void test();

	virtual void applyInvA(Vec in, IterationManager *itManager);
	virtual void applyPC(Vec g, Vec z);
	virtual void applyPrimalMult(Vec in, Vec out);
	void removeNullSpace(Vec in);
	virtual ASolver* instanceOuterSolver(Vec d, Vec lmb);
	virtual void setRequiredPrecision(PetscReal reqPrecision);
};

#endif