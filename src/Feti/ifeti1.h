#ifndef JAPET_FETI_IFETI1_H
#define JAPET_FETI_IFETI1_H

#include "Feti/feti1.h"

class iFeti1: public Feti1 {

public:
	iFeti1(PDCommManager *comMan, Mat A, Vec b, Mat BT, Mat B, Vec lmb,
			NullSpaceInfo *nullSpace, PetscInt localNodeCount, PetscInt fNodesCount,
			PetscInt *fNodes, CoarseProblemMethod cpM = ParaCG) :
				Feti1(comMan, A, b, BT, B, lmb, nullSpace, localNodeCount, fNodesCount, fNodes, cpM) {
	}

	virtual ASolver* instanceOuterSolver(Vec d, Vec lmb);

	virtual void setRequiredPrecision(PetscReal reqPrecision) {
		//PetscPrintf(PETSC_COMM_WORLD, "PRECSET: %e \n", reqPrecision);
		PetscReal prec = fmax(reqPrecision, 1e-15);
		KSPSetTolerances(kspA, prec, prec, 1e10, 1000);
	}
};

#endif