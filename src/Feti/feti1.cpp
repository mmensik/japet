#include "feti1.h"

#include "Utils/logger.h"

Feti1::Feti1(PDCommManager *comMan, Mat A, Vec b, Mat BT, Mat B, Vec lmb,
		NullSpaceInfo *nullSpace, PetscInt localNodeCount, PetscInt fNodesCount,
		PetscInt *fNodes, CoarseProblemMethod cpM, SystemR *sR) :
		AFeti(comMan, b, BT, B, lmb, nullSpace, cpM, sR) {

	if (cMan->isPrimal()) {

		PetscInt lNodeCount;
		VecGetLocalSize(b, &lNodeCount);

		//Sestaveni Nuloveho prostoru lokalni casti matice tuhosti A
		if (isLocalSingular) {
			MatNullSpaceCreate(PETSC_COMM_SELF, PETSC_TRUE, nullSpace->localDimension, nullSpace->localBasis, &locNS);
		}

		this->A = A;

		Aloc = A;
		//extractLocalAPart(A, &Aloc);
		//Matrix regularization!

		PetscInt firstRow, lastRow, nCols, locNullDim, nodeDim;

		VecGetOwnershipRange(b, &firstRow, &lastRow);
		MatGetSize(R, PETSC_NULL, &nCols);

		locNullDim = nCols / cMan->getPrimalSize();
		nodeDim = (lastRow - firstRow) / localNodeCount; //Number of rows for each node

		PetscInt FIX_NODE_COUNT;
		PetscInt fixingNodes[5];

		if (fNodesCount == 0) {
			if (nodeDim == 1) { //Laplace
				FIX_NODE_COUNT = 1;
				fixingNodes[0] = (firstRow / nodeDim + lastRow / nodeDim - 1) / 2;
			} else if (nodeDim == 2) { //Elasticity
				FIX_NODE_COUNT = 5;
				fixingNodes[0] = firstRow / nodeDim + 4;
				fixingNodes[1] = lastRow / nodeDim - 4;
				fixingNodes[2] = (firstRow / nodeDim + lastRow / nodeDim - 1) / 2 + 5;
				fixingNodes[3] = (firstRow / nodeDim + lastRow / nodeDim - 1) / 2 - 5;
				fixingNodes[4] = (firstRow / nodeDim + lastRow / nodeDim - 1) / 2;

				//PetscPrintf(PETSC_COMM_SELF, "%d %d %d %d %d \n", fixingNodes[0], fixingNodes[1], fixingNodes[2], fixingNodes[3], fixingNodes[4]);
			}
		} else {
			for (int i = 0; i < fNodesCount; i++) {
				fixingNodes[i] = fNodes[i] + firstRow / nodeDim;
			}
		}

		Mat REG;
		MatCreateSeqAIJ(PETSC_COMM_SELF, lastRow - firstRow, locNullDim, FIX_NODE_COUNT
				* nodeDim * locNullDim, PETSC_NULL, &REG);

		PetscReal values[locNullDim];
		PetscInt idx[locNullDim];
		for (int i = 0; i < locNullDim; i++) {
			idx[i] = cMan->getPrimalRank() * locNullDim + i;
		}

		for (int i = 0; i < FIX_NODE_COUNT; i++)
			for (int d = 0; d < nodeDim; d++) {

				PetscInt colInd = fixingNodes[i] * nodeDim + d;
				MatGetValues(R, 1, &colInd, locNullDim, idx, values);

				for (int j = 0; j < locNullDim; j++) {
					MatSetValue(REG, colInd - firstRow, j, values[j], INSERT_VALUES);
				}

			}

		MatAssemblyBegin(REG, MAT_FINAL_ASSEMBLY);
		MatAssemblyEnd(REG, MAT_FINAL_ASSEMBLY);

		Mat REGREGT;
		Mat REGT;
		MatTranspose(REG, MAT_INITIAL_MATRIX, &REGT);
		MatMatMult(REG, REGT, MAT_INITIAL_MATRIX, 1, &REGREGT);

		//MatView(REGREGT, PETSC_VIEWER_STDOUT_SELF);

		Mat Areg;
		MatDuplicate(Aloc, MAT_COPY_VALUES, &Areg);

		MatAXPY(Areg, 1, REGREGT, DIFFERENT_NONZERO_PATTERN);
		/*
		 std::stringstream ss2;
		 ss2 << "../matlab/data/Ar" << cMan->getPrimalRank() << ".m";

		 int rank;

		 PetscViewer v;
		 PetscViewerBinaryOpen(PETSC_COMM_SELF, ss2.str().c_str(), FILE_MODE_WRITE, &v);
		 MatView(Areg, v);
		 PetscViewerDestroy(v);
		 */
		PC pc;

		MyLogger::Instance()->getTimer("Factorization")->startTimer();
		{

			PCCreate(PETSC_COMM_SELF, &pc);
			PCSetOperators(pc, Areg, Areg, SAME_PRECONDITIONER);
			PCSetFromOptions(pc);
			PCSetUp(pc);

			KSPCreate(PETSC_COMM_SELF, &kspA);
			KSPSetTolerances(kspA, 1e-15, 1e-15, 1e8, 1000);
			KSPSetPC(kspA, pc);
			KSPSetOperators(kspA, Areg, Areg, SAME_PRECONDITIONER);
		}
		MyLogger::Instance()->getTimer("Factorization")->stopTimer();

		MatDestroy(Areg);
		MatDestroy(REGREGT);
		MatDestroy(REGT);

		PCDestroy(pc);

		if (isLocalSingular) KSPSetNullSpace(kspA, locNS);

		VecCreateGhost(cMan->getPrimal(), lNodeCount, PETSC_DECIDE, 0, PETSC_NULL, &tempInv);
		VecSet(tempInv, 0);
		VecGhostGetLocalForm(tempInv, &tempInvGh);
		VecDuplicate(tempInvGh, &tempInvGhB);

	}

	//PetscPrintf(PETSC_COMM_WORLD, "FETI1 const. done \n");

}

void Feti1::solve() {

	AFeti::solve();

	PetscReal normB, error;

	if (isVerbose) {

		VecNorm(b, NORM_2, &normB);

		applyPrimalMult(u, temp);
		VecAXPY(temp, -1, b);
		MatMult(BT, lmb, tempInv);
		VecAXPY(temp, 1, tempInv);

		VecNorm(temp, NORM_2, &error);

		PetscPrintf(cMan->getPrimal(), "Relative error: %e\n\n", error / normB);

	}

}

Feti1::~Feti1() {

	if (cMan->isPrimal()) {
		KSPDestroy(kspA);
		MatDestroy(Aloc);
		if (isLocalSingular) {
			MatNullSpaceDestroy(locNS);
		}
		VecDestroy(tempInv);
		VecDestroy(tempInvGh);
		VecDestroy(tempInvGhB);
	}

}

void Feti1::applyInvA(Vec in, IterationManager *itManager) {

	VecCopy(in, tempInv);

	MatNullSpaceRemove(locNS, tempInvGh, PETSC_NULL);

	//KSPSetTolerances(kspA, precision, precision, 1e8, 100);
	KSPSolve(kspA, tempInvGh, tempInvGhB);

	MatNullSpaceRemove(locNS, tempInvGhB, PETSC_NULL);

	PetscInt itNumber;
	KSPGetIterationNumber(kspA, &itNumber);

	inIterations += itNumber;

	VecCopy(tempInvGhB, tempInvGh);
	VecCopy(tempInv, in);

	if (itManager != NULL) {
		itManager->setIterationData("InCG.count", itNumber);
	}
}

void Feti1::applyPC(Vec g, Vec z) {

	if (cMan->isPrimalRoot()) MyLogger::Instance()->getTimer("F^-1")->startTimer();
	MatMult(BT, g, temp);
	applyPrimalMult(temp, temp);
	MatMult(B, temp, z);
	if (cMan->isPrimalRoot()) MyLogger::Instance()->getTimer("F^-1")->stopTimer();

	//VecCopy(g, z);
}

void Feti1::applyPrimalMult(Vec in, Vec out) {

	VecCopy(in, tempInv);

	MatMult(Aloc, tempInvGh, tempInvGhB);
	VecCopy(tempInvGhB, tempInvGh);

	VecCopy(tempInv, out);

}
