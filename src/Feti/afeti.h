#ifndef JAPET_FETI_AFETI_H
#define JAPET_FETI_AFETI_H


#include "Fem/fem.h"

 #include "Solvers/asolver.h"
 #include "Solvers/cgsolver.h"

class GGLinOp: public SolverApp, public SolverCtr {
	Mat B;
	Mat R;

	Vec temp1, temp2, temp3;
public:
	GGLinOp(Mat B, Mat R);
	virtual void applyMult(Vec in, Vec out, IterationManager *info);
	virtual bool
	isConverged(PetscInt itNumber, PetscReal norm, PetscReal bNorm, Vec *vec);
};



/**
 * @brief Abstract FETI ancestor
 * @note		There is still some work to do. The messiest part of implementation is in dealing with
 object G matrix and computation of the projector P = I - G inv(G'G) G'
 *
 **/


class AFeti: public SolverApp,
		public SolverCtr,
		public SolverPreconditioner,
		public SolverProjector {
protected:

	PDCommManager *cMan;

	ASolver *outerSolver;
	bool isVerbose;

	//
	// DUAL
	//

	VecScatter tgScat; ///< Scatter from dual group to master. For application of inv G'G
	Vec tgLocIn, tgLocOut;
	Vec tgA;
	Vec tgB;

	CGSolver *ggParSol;
	Vec parT1, parT2;

	Vec lmb; ///< Lambda vector
	Vec d; ///< dual right side
	Vec e;

	Vec lmbKerPrev;

	PetscInt gM, gN; ///<	dimensions of G
	Mat G, GT; ///< BR

	Mat Groot;

	KSP kspG; ///< Global G'G solver
	MatNullSpace GTGNullSpace;

	VecScatter dBScat; ///< Scatter from dual group to master
	Vec dBGlob; ///< gloval version
	Vec dBLoc; ///< local (on root) version

	//
	// PRIMAL
	//

	Mat BT;
	Mat B; ///< Jump operator matrix
	Vec b; ///< Global force vector
	Vec u; ///< solution
	Mat R; ///< Global null space of A

	SystemR *systemR; ///< NullSpace of the whole system - even with constrains
	MatNullSpace systemNullSpace;

	Vec temp;
	Vec tempLoc;

	VecScatter pBScat; ///< Scatter from primal to primal root
	Vec pBGlob; ///< gloval version
	Vec pBLoc; ///< local (on root) version


	bool isSingular; ///< is Matrix A singular
	bool isLocalSingular; ///< is local part of A singular


	PetscReal lastNorm; ///< last computed norm

	PetscReal precision;

	PetscInt outIterations;
	PetscInt inIterations;

	PetscLogStage coarseStage, coarseInitStage, aInvStage, fetiInitStage;
	CoarseProblemMethod cpMethod;

	void initCoarse();

	void applyInvGTG(Vec in, Vec out);

public:

	//
	// Constants for identification and synchronization during the dual-primal solving
	//
	const static int P_ACTION_INVA = 1;
	const static int P_ACTION_MULTA = 2;
	const static int P_ACTION_BREAK = -1;

	AFeti(PDCommManager *comMan, Vec b, Mat BT, Mat B, Vec lmb,
			NullSpaceInfo *nullSpace, CoarseProblemMethod mcpM = ParaCG,
			SystemR *sR = PETSC_NULL);
	virtual ~AFeti();

	virtual void applyInvA(Vec in, IterationManager *itManager) = 0;
	virtual void applyPrimalMult(Vec in, Vec out);
	virtual void applyProjection(Vec v, Vec pv);
	void solve(Vec b) {
		this->b = b;
		solve();
	}
	virtual void solve();
	virtual void applyMult(Vec in, Vec out, IterationManager *info);
	virtual bool
	isConverged(PetscInt itNumber, PetscReal norm, PetscReal bNorm, Vec *vec);

	virtual ASolver* instanceOuterSolver(Vec d, Vec l);
	virtual void applyPC(Vec g, Vec z) {
		projectGOrth(g);
		VecCopy(g, z);
	}

	void dumpSolution(PetscViewer v);
	void dumpSystem(PetscViewer v);
	void projectGOrth(Vec in); ///< Remove space spaned by G from vec in
	void copySolution(Vec out); /// <Copy solution to vector out
	void copyLmb(Vec out);
	void setPrec(PetscReal prec) {
		precision = prec;
	}
	void setIsVerbose(bool verbose) {
		isVerbose = verbose;
	}
	void saveIterationInfo(const char *fileName) {
		outerSolver->saveIterationInfo(fileName);
	}

	void testSomething();

	void setSystemSingular() {

		PetscPrintf(PETSC_COMM_SELF, "CODE TO WRITE you Moorons!!! setSystemSingular \n");
		//
		// FIX
		//

		//MatNullSpace NS;
		//MatNullSpaceCreate(comm, PETSC_TRUE, 0, PETSC_NULL, &NS);
		//KSPSetNullSpace(kspG, NS);
	}

	PetscInt getOutIterations() {
		return outIterations;
	}
	PetscInt getInIterations() {
		return inIterations;
	}
};

#endif