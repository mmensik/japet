#include "ifeti1.h"


#include "Solvers/steepestdescent.h"
#include "Solvers/bbsolver.h"
#include "Solvers/asinsolver.h"

ASolver* iFeti1::instanceOuterSolver(Vec d, Vec l) {
	//return new CGSolver(this, d, l, this);

	if (outerSolver == NULL) {

		switch (ConfigManager::Instance()->innerSolver) {

		case STEEPEST_DESCENT:
			outerSolver = new SteepestDescent(this);
			break;
		case BB:
			outerSolver = new BBSolver(this);
			break;
		case ASIN:
			outerSolver = new ASinSolver(this);
			break;
		default:
			outerSolver = new BBSolver(this);
			break;
		}

		outerSolver->setPreconditioner(this);
		outerSolver->setProjector(this);
	}

	KSPSetTolerances(kspA, 1e-20, 1e-20, 1e8, 300);

	return outerSolver;
}
