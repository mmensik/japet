#include "afeti.h"

#include "Utils/timer.h"
#include "Utils/logger.h"

GGLinOp::GGLinOp(Mat B, Mat R) {
	this->B = B;
	this->R = R;

	MatGetVecs(B, NULL, &temp2);
	MatGetVecs(R, NULL, &temp1);
}

void GGLinOp::applyMult(Vec in, Vec out, IterationManager *info) {

	MatMult(R, in, temp1);
	MatMult(B, temp1, temp2);
	MatMultTranspose(B, temp2, temp1);
	MatMultTranspose(R, temp1, out);
}

bool GGLinOp::isConverged(PetscInt itNumber, PetscReal norm, PetscReal bNorm,
		Vec *vec) {
	return norm / bNorm < 1e-12;
}

AFeti::AFeti(PDCommManager* comMan, Vec b, Mat BT, Mat B, Vec lmb,
		NullSpaceInfo *nullSpace, CoarseProblemMethod cpM, SystemR *sR) {

	this->cpMethod = cpM;
	this->cMan = comMan;

	outerSolver = NULL;
	isVerbose = false;

	isSingular = true; /// FIX - only works for total FETI

	this->b = b;
	this->BT = BT;
	this->B = B;

	this->systemR = sR;

	if (sR != PETSC_NULL && sR->rDim > 0) {
		MatNullSpaceCreate(comMan->getPrimal(), PETSC_TRUE, sR->rDim, sR->systemGNullSpace, &systemNullSpace);
	} else {
		systemNullSpace = PETSC_NULL;
	}

	isLocalSingular = nullSpace->isSubDomainSingular;
	isSingular = nullSpace->isDomainSingular;
	R = nullSpace->R;

	precision = 1e-4;

	PetscInt lNodeCount;
	VecGetLocalSize(b, &lNodeCount);

	//Priprava ghostovaneho vektoru TEMP a kopie vektoru b do lokalnich casti bloc
	VecCreateGhost(cMan->getPrimal(), lNodeCount, PETSC_DECIDE, 0, PETSC_NULL, &temp);
	//VecCopy(b, temp);
	VecGhostGetLocalForm(temp, &tempLoc);

	//Ghostovany vektor reseni
	VecCreateGhost(cMan->getPrimal(), lNodeCount, PETSC_DECIDE, 0, PETSC_NULL, &u);

	PetscInt dualSize;
	MatGetSize(B, &dualSize, PETSC_NULL);
	VecCreateMPI(cMan->getPrimal(), PETSC_DECIDE, dualSize, &pBGlob);
	//VecScatterCreateToZero(pBGlob, &pBScat, &pBLoc);
	VecScatterCreateToZero(lmb, &dBScat, &dBLoc);

	this->lmb = lmb;

	VecDuplicate(lmb, &lmbKerPrev);
	VecSet(lmbKerPrev, 0);

	//If the matrix A is singular, the matrix G and G'G has to be prepared.
	if (isSingular) initCoarse();

}

void AFeti::initCoarse() {
	//PetscInt rank;

	Mat GTemp, GTGloc;

	MyLogger::Instance()->getTimer("Coarse init")->startTimer();
	//	MyTimer* timer = MyLogger::Instance()->getTimer("Coarse init");

	PetscInt nD;
	MatGetSize(R, PETSC_NULL, &nD);
	VecCreateMPI(cMan->getPrimal(), PETSC_DECIDE, nD, &e);

	GGLinOp *linOp;

	switch (cpMethod) {
	case ParaCG:

		gN = nD;
		MatGetVecs(R, &parT2, &parT1);

		VecSet(parT1, 0);
		VecSet(parT2, 0);

		linOp = new GGLinOp(B, R);
		ggParSol = new CGSolver(linOp, NULL, cMan->getDual());
		ggParSol->setSolverCtr(linOp);

		if (systemR != PETSC_NULL && systemR->rDim > 0) {
			MatNullSpaceCreate(cMan->getDual(), PETSC_TRUE, systemR->rDim, systemR->systemGNullSpace, &GTGNullSpace);
		}

		VecDuplicate(parT2, &tgA);
		VecDuplicate(tgA, &tgB);

		break;

	case MasterWork:

		MatMatMult(B, R, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &G);

		MatGetLocalMat(G, MAT_INITIAL_MATRIX, &GTemp);

		PetscScalar *val;
		PetscInt *ia, *ja;
		PetscInt n;
		PetscTruth done;
		PetscInt lm, ln;

		MatGetSize(GTemp, &lm, &ln);
		MatGetArray(GTemp, &val);
		MatGetRowIJ(GTemp, 0, PETSC_FALSE, PETSC_FALSE, &n, &ia, &ja, &done);

		if (cMan->isPrimalRoot()) {

			PetscInt lNumRow[cMan->getPrimalSize()], lNNZ[cMan->getPrimalSize()],
					firstRowIndex[cMan->getPrimalSize() + 1],
					displ[cMan->getPrimalSize()], totalNNZ, totalRows;
			MPI_Gather(&n, 1, MPI_INT, lNumRow, 1, MPI_INT, 0, cMan->getPrimal());
			MPI_Gather(&ia[n], 1, MPI_INT, lNNZ, 1, MPI_INT, 0, cMan->getPrimal());

			totalNNZ = 0;
			totalRows = 0;

			for (int i = 0; i < cMan->getPrimalSize(); i++) {
				firstRowIndex[i] = totalRows;
				displ[i] = totalNNZ;
				totalNNZ += lNNZ[i];
				totalRows += lNumRow[i];
			}
			firstRowIndex[cMan->getPrimalSize()] = totalRows;

			PetscInt *locJA = new PetscInt[totalNNZ];
			PetscInt *locIA = new PetscInt[totalRows + 1];
			PetscScalar *locVal = new PetscScalar[totalNNZ];

			locIA[0] = 0;
			MPI_Gatherv(ja, ia[n], MPI_INT, locJA, lNNZ, displ, MPI_INT, 0, cMan->getPrimal());
			MPI_Gatherv(ia + 1, n, MPI_INT, locIA + 1, lNumRow, firstRowIndex, MPI_INT, 0, cMan->getPrimal());
			MPI_Gatherv(val, ia[n], MPI_DOUBLE, locVal, lNNZ, displ, MPI_DOUBLE, 0, cMan->getPrimal());

			for (int j = 0; j < cMan->getPrimalSize(); j++) {
				for (int i = firstRowIndex[j] + 1; i < firstRowIndex[j + 1] + 1; i++) {
					locIA[i] += displ[j];
				}
			}

			MatCreateSeqAIJWithArrays(PETSC_COMM_SELF, totalRows, ln, locIA, locJA, locVal, &Groot);

			MatMatMultTranspose(Groot, Groot, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &GTGloc);

			//MatDestroy(Groot);
			//delete[] locJA;
			//delete[] locIA;
			//delete[] locVal;

			PC pcGTG;
			PCCreate(PETSC_COMM_SELF, &pcGTG);
			PCSetOperators(pcGTG, GTGloc, GTGloc, SAME_PRECONDITIONER);
			KSPCreate(PETSC_COMM_SELF, &kspG);
			KSPSetOperators(kspG, GTGloc, GTGloc, SAME_PRECONDITIONER);

			MatDestroy(GTGloc);

			if (systemR == PETSC_NULL || systemR->rDim == 0) {
				PCSetType(pcGTG, "lu");

				PCSetUp(pcGTG);

				KSPSetPC(kspG, pcGTG);
				PCDestroy(pcGTG);

			} else {
				//KSPSetTolerances(kspG, 1e-18, 1e-18, 1e10, 550);
			}

		} else {
			MPI_Gather(&n, 1, MPI_INT, PETSC_NULL, 1, MPI_INT, 0, cMan->getPrimal());
			MPI_Gather(&ia[n], 1, MPI_INT, PETSC_NULL, 1, MPI_INT, 0, cMan->getPrimal());

			MPI_Gatherv(ja, ia[n], MPI_INT, NULL, NULL, NULL, MPI_INT, 0, cMan->getPrimal());
			MPI_Gatherv(ia + 1, n, MPI_INT, NULL, NULL, NULL, MPI_INT, 0, cMan->getPrimal());
			MPI_Gatherv(val, ia[n], MPI_DOUBLE, NULL, NULL, NULL, MPI_DOUBLE, 0, cMan->getPrimal());
		}

		MatRestoreRowIJ(GTemp, 0, PETSC_FALSE, PETSC_FALSE, &n, &ia, &ja, &done);
		MatRestoreArray(GTemp, &val);

		MatDestroy(GTemp);
		MatGetSize(G, &gM, &gN);

		VecCreateMPI(cMan->getDual(), PETSC_DECIDE, gN, &tgA);
		VecDuplicate(tgA, &tgB);

		VecScatterCreateToZero(tgA, &tgScat, &tgLocIn);
		VecDuplicate(tgLocIn, &tgLocOut);

		if (systemR != PETSC_NULL && systemR->rDim > 0) {
			Vec gNullSpace[systemR->rDim];
			for (int i = 0; i < systemR->rDim; i++) {

				VecCopy(systemR->systemGNullSpace[i], tgA);

				VecScatterBegin(tgScat, tgA, tgLocIn, INSERT_VALUES, SCATTER_FORWARD);
				VecScatterEnd(tgScat, tgA, tgLocIn, INSERT_VALUES, SCATTER_FORWARD);

				if (cMan->isDualRoot()) {
					VecDuplicate(tgLocIn, &(gNullSpace[i]));
					VecCopy(tgLocIn, gNullSpace[i]);
				}
			}

			if (cMan->isDualRoot()) {
				MatNullSpaceCreate(PETSC_COMM_SELF, PETSC_FALSE, systemR->rDim, gNullSpace, &GTGNullSpace);
				KSPSetNullSpace(kspG, GTGNullSpace);
			}
		}

		break;

	default:
		break;
	}
	MyLogger::Instance()->getTimer("Coarse init")->stopTimer();

	if (cpMethod != ParaCG) MatScale(G, -1);

}

void AFeti::applyInvGTG(Vec in, Vec out) {

	switch (cpMethod) {
	case ParaCG:

		if (systemR != PETSC_NULL && systemR->rDim > 0) MatNullSpaceRemove(GTGNullSpace, in, PETSC_NULL);

		ggParSol->solve(in, out);

		if (systemR != PETSC_NULL && systemR->rDim > 0) MatNullSpaceRemove(GTGNullSpace, out, PETSC_NULL);

		break;
	case MasterWork:

		VecScatterBegin(tgScat, in, tgLocIn, INSERT_VALUES, SCATTER_FORWARD);
		VecScatterEnd(tgScat, in, tgLocIn, INSERT_VALUES, SCATTER_FORWARD);

		if (cMan->isDualRoot()) {
			KSPSetTolerances(kspG, 1e-19, 1e-19, 1e7, 2);
			if (systemR != PETSC_NULL && systemR->rDim > 0) MatNullSpaceRemove(GTGNullSpace, tgLocIn, PETSC_NULL);
			KSPSolve(kspG, tgLocIn, tgLocOut);
			if (systemR != PETSC_NULL && systemR->rDim > 0) MatNullSpaceRemove(GTGNullSpace, tgLocOut, PETSC_NULL);
		}

		VecScatterBegin(tgScat, tgLocOut, out, INSERT_VALUES, SCATTER_REVERSE);
		VecScatterEnd(tgScat, tgLocOut, out, INSERT_VALUES, SCATTER_REVERSE);
		break;
	case ORTO:
		VecCopy(in, out);
		break;
	}

	//if (systemR != PETSC_NULL && systemR->rDim > 0) MatNullSpaceRemove(GTGNullSpace, out, PETSC_NULL);

}

void AFeti::projectGOrth(Vec in) {

	MyLogger::Instance()->getTimer("Coarse problem")->startTimer();

	if (cpMethod != ParaCG) {
		MatMultTranspose(G, in, tgA);
	} else {

		MatMultTranspose(B, in, parT1);
		MatMultTranspose(R, parT1, tgA);

	}

	applyInvGTG(tgA, tgB);

	VecScale(in, -1);

	if (cpMethod != ParaCG) {
		MatMultAdd(G, tgB, in, in);
	} else {
		MatMult(R, tgB, parT1);
		MatMultAdd(B, parT1, in, in);
	}
	VecScale(in, -1);
	MyLogger::Instance()->getTimer("Coarse problem")->stopTimer();
}

AFeti::~AFeti() {

	if (cMan->isPrimal()) {
		VecDestroy(u);
		VecDestroy(temp);
		VecDestroy(tempLoc);

		if (isSingular) {
			MatDestroy(R);
		}

		//VecScatterDestroy(pBScat);
	}

	if (cMan->isDual()) {
		MatDestroy(G);
		VecDestroy(lmb);
		if (cMan->isDualRoot()) {
			KSPDestroy(kspG);
		}

		VecScatterDestroy(dBScat);
		VecScatterDestroy(tgScat);

		VecDestroy(tgA);
		VecDestroy(tgB);

		VecDestroy(tgLocIn);
		VecDestroy(tgLocOut);
	}

	if (outerSolver != NULL) delete outerSolver;

}

void AFeti::dumpSolution(PetscViewer v) {
	VecView(u, v);
	VecView(lmb, v);
}

void AFeti::dumpSystem(PetscViewer v) {
	//View A
	//VecView(b, v);
	//MatView(B, v);
}

void AFeti::applyMult(Vec in, Vec out, IterationManager *info) {

	outIterations++;

	if (cMan->isPrimalRoot()) MyLogger::Instance()->getTimer("BA+BT")->startTimer();

	MatMult(BT, in, temp);

	PetscReal tNorm;
	VecNorm(temp, NORM_2, &tNorm);

	VecScale(temp, 1 / tNorm);
	applyInvA(temp, info);
	VecScale(temp, tNorm);

	MatMult(B, temp, out);

	//PetscPrintf(PETSC_COMM_WORLD, "%e\t%e\t%e\n", v1, v2, v3);

	if (cMan->isPrimalRoot()) MyLogger::Instance()->getTimer("BA+BT")->stopTimer();

}

void AFeti::applyProjection(Vec v, Vec pv) {
	if (v != pv) VecCopy(v, pv);
	projectGOrth(pv);
}

void AFeti::applyPrimalMult(Vec in, Vec out) {
	VecCopy(in, out);
}

ASolver* AFeti::instanceOuterSolver(Vec d, Vec l) {
	//return new CGSolver(this, d, l, this);

	if (outerSolver == NULL) {
		outerSolver = new CGSolver(this);
		outerSolver->setPreconditioner(this);
		outerSolver->setProjector(this);
	}

	return outerSolver;
}

void AFeti::solve() {

	inIterations = 0;
	outIterations = 0;

	Vec d;
	//	VecScatter peScat, deScat;

	VecDuplicate(lmb, &d);

	VecCopy(b, temp);

	applyInvA(temp, NULL);

	//VecView(temp, PETSC_VIEWER_STDOUT_SELF);

	MatMult(B, temp, d);

	//Preparation of the right-hand side vector d=PBA^+b
	//The matrix P is projector on the space orthogonal to range(G)

	//projectGOrth(d); //Projection

	//Feasible lambda_0 preparation

	Vec lmbKer, dAlt;

	if (isSingular) { //Je li singularni, je treba pripavit vhodne vstupni lambda

		MatMultTranspose(R, b, e);

		//VecView(b, PETSC_VIEWER_STDOUT_WORLD);

		VecScale(e, -1);

		Vec eTemp;
		VecDuplicate(e, &eTemp);

		applyInvGTG(e, eTemp);

		if (cpMethod != ParaCG) {
			MatMult(G, eTemp, lmb);
		} else {
			MatMult(R, eTemp, parT1);
			MatMult(B, parT1, lmb);
			VecScale(lmb, -1);
		}

		VecDuplicate(lmb, &lmbKer);
		VecDuplicate(d, &dAlt);
		applyMult(lmb, lmbKer, NULL);

		VecCopy(d, dAlt);
		VecAXPY(dAlt, -1, lmbKer);

		VecSet(lmbKer, 0);

		projectGOrth(dAlt);

	}

	//	PetscViewer v;
	//	PetscViewerBinaryOpen(cMan->getPrimal(), "../matlab/dTemp.m", FILE_MODE_WRITE, &v);
	//	VecView(dAlt, v);
	//	PetscViewerDestroy(v);

	outerSolver = instanceOuterSolver(dAlt, lmbKerPrev);
	outerSolver->setSolverCtr(this);
	outerSolver->setIsVerbose(isVerbose);

	//Solve!!!
	outerSolver->solve(dAlt, lmbKer);

	projectGOrth(lmbKer);

	VecAXPY(lmb, 1, lmbKer);

	VecCopy(lmbKer, lmbKerPrev);

	//
	// Rigid body motions
	//
	if (isSingular) {

		Vec lmbTemp;
		VecDuplicate(lmb, &lmbTemp);
		applyMult(lmb, lmbTemp, NULL);

		VecAYPX(lmbTemp, -1, d);

		Vec bAlp, alpha;

		VecCreateMPI(cMan->getDual(), PETSC_DECIDE, gN, &bAlp);
		VecDuplicate(bAlp, &alpha);

		if (cpMethod == ParaCG) {

			MatMultTranspose(B, lmbTemp, parT1);
			MatMultTranspose(R, parT1, bAlp);

			VecScale(bAlp, -1);
			applyInvGTG(bAlp, alpha);

		} else {
			MatMultTranspose(G, lmbTemp, bAlp);
			applyInvGTG(bAlp, alpha);
		}

		VecCopy(b, u);

		VecScale(lmb, -1);
		MatMultAdd(BT, lmb, u, u);
		VecScale(lmb, -1);
		applyInvA(u, NULL);
		MatMultAdd(R, alpha, u, u);

	}

	//VecScale(lmb, -1);
	//VecDestroy(d);

	if (isVerbose) {
		PetscReal feasErr, uNorm;

		MatMult(B, u, d);

		VecNorm(d, NORM_2, &feasErr);
		VecNorm(u, NORM_2, &uNorm);

		PetscPrintf(cMan->getPrimal(), "\n");
		PetscPrintf(cMan->getPrimal(), "FETI finished   Outer it: %d   Inner it: %d\n", outIterations, inIterations);
		PetscPrintf(cMan->getPrimal(), "Feasibility err: %e \n", feasErr / uNorm);
	}
}

void AFeti::copySolution(Vec out) {

	VecCopy(u, out);
}

void AFeti::copyLmb(Vec out) {

	VecCopy(lmb, out);
}

void AFeti::testSomething() {

}

bool AFeti::isConverged(PetscInt itNumber, PetscReal norm, PetscReal bNorm,
		Vec *vec) {
	lastNorm = norm;

	return norm / bNorm < precision || itNumber > 1000;
}