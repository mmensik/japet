#ifndef JAPET_FETI_FFETI_H
#define JAPET_FETI_FFETI_H

#include "Feti/feti1.h"

class FFeti: public Feti1 {
protected:
	Mat F;

	VecScatter fToRoot;
	Vec fLocal, fGlobal;

	KSP kspF;
	KSP kspS;
public:
	FFeti(PDCommManager *comMan, Mat A, Vec b, Mat BT, Mat B, Vec lmb,
			NullSpaceInfo *nullSpace, PetscInt localNodeCount, PetscInt fNodesCount,
			PetscInt *fNodes, CoarseProblemMethod cpM = ParaCG,
			SystemR *sR = PETSC_NULL);

	virtual void solve();

	virtual void applyMult(Vec in, Vec out, IterationManager *info);

	//	virtual void solve();

};

#endif