/**	@file		feti.h
 @brief	FETI Method
 @author Martin Mensik
 @date 	2010
 */

#ifndef FETI_H
#define FETI_H

#include <math.h>
#include <map>
#include <set>
#include <string>
#include <sstream>

#include "petscksp.h"
#include "petscmat.h"


#include "Feti/afeti.h"

#include "Fem/fem.h"

#include "Feti/feti1.h"

void GenerateJumpOperator(Mesh *mesh, Mat &B, Vec &lmb);

void GenerateTotalJumpOperator(Mesh *mesh, int d, Mat &B, Mat &BT, Vec &lmb,
		PDCommManager* commManager);

void GenerateClusterJumpOperator(Mesh *mesh, SubdomainCluster *cluster,
		Mat &BGlob, Mat &BTGlob, Vec &lmbGlob, Mat &BCluster, Mat &BTCluster,
		Vec &lmbCluster, MPI_Comm comm);

void Generate2DLaplaceNullSpace(Mesh *mesh, bool &isSingular,
		bool &isLocalSingular, Mat *Rmat, MPI_Comm comm = PETSC_COMM_WORLD);

void Generate2DLaplaceTotalNullSpace(Mesh *mesh, NullSpaceInfo *nullSpace,
		MPI_Comm comm = PETSC_COMM_WORLD);

void Generate2DElasticityNullSpace(Mesh *mesh, NullSpaceInfo *nullSpace,
		MPI_Comm comm = PETSC_COMM_WORLD);

void Generate2DLaplaceClusterNullSpace(Mesh *mesh, SubdomainCluster *cluster);

void
Generate2DElasticityClusterNullSpace(Mesh *mesh, SubdomainCluster *cluster,
		MPI_Comm comm);

void getLocalJumpPart(Mat B, Mat *Bloc);

void Generate2DTotalJumpOperatorRECT(PetscReal x0, PetscReal x1, PetscReal y0,
		PetscReal y1, PetscReal h, PetscInt m, PetscInt n, Mat BT, MPI_Comm comm);

Feti1* createFeti(Mesh *mesh, PetscReal(*f)(Point), PetscReal(*K)(Point),
		MPI_Comm comm);

class JumpRectMatrix {
	PetscReal h;
	PetscInt m;
	PetscInt n;

	PetscInt xEdges;
	PetscInt yEdges;

	PetscInt nDirchlets;
	PetscInt nCorners;
	PetscInt nPairs;
	PetscInt bRows;
public:
	JumpRectMatrix(PetscReal x0, PetscReal x1, PetscReal y0,
			PetscReal y1, PetscReal h, PetscInt m, PetscInt n);

	PetscInt getGlobalIndex(PetscInt subDom, PetscInt x, PetscInt y);

	std::map<PetscInt, PetscReal> getRow(PetscInt row);
};

#endif
