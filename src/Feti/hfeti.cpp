#include "hfeti.h"

#include "Feti/ffeti.h"

HFeti::HFeti(PDCommManager* pdMan, Mat A, Vec b, Mat BGlob, Mat BTGlob,
		Mat BClust, Mat BTClust, Vec lmbGl, Vec lmbCl, SubdomainCluster *cluster,
		PetscInt localNodeCount) :
		AFeti(pdMan, b, BTGlob, BGlob, lmbGl, cluster->outerNullSpace, MasterWork) {

	VecCreateGhost(cMan->getParen(), localNodeCount * 2, PETSC_DECIDE, 0, PETSC_NULL, &globTemp);
	VecSet(globTemp, 0);
	VecGhostGetLocalForm(globTemp, &globTempGh);

	VecCreateGhost(cluster->clusterComm, localNodeCount * 2, PETSC_DECIDE, 0, PETSC_NULL, &clustTemp);
	VecSet(clustTemp, 0);
	VecGhostGetLocalForm(clustTemp, &clustTempGh);

	VecCreateMPI(cluster->clusterComm, localNodeCount * 2, PETSC_DECIDE, &clustb);

	VecCopy(b, globTemp);
	VecCopy(globTempGh, clustTempGh);
	VecCopy(clustTemp, clustb);

	this->A = A;

	this->cluster = cluster;

	precision = 5e-4;

	PDCommManager *clustComMan =
			new PDCommManager(cluster->clusterComm, SAME_COMMS);

	//
	// TODO GTG ve vnitrnim feti neni regularni!!!
	//

	subClusterSystem =
			new FFeti(clustComMan, A, clustb, BTClust, BClust, lmbCl, cluster->clusterNullSpace, localNodeCount, 0, NULL, MasterWork, &(cluster->clusterR));

	subClusterSystem->setPrec(1e-8);

	//Sestaveni Nuloveho prostoru lokalni casti matice tuhosti A
	MatNullSpaceCreate(cluster->clusterComm, PETSC_TRUE, 3, cluster->outerNullSpace->localBasis, &clusterNS);

	PetscInt lNodeCount;

	VecGetLocalSize(b, &lNodeCount);
	VecCreateGhost(cMan->getPrimal(), lNodeCount, PETSC_DECIDE, 0, PETSC_NULL, &tempInv);
	VecSet(tempInv, 0);
	VecGhostGetLocalForm(tempInv, &tempInvGh);
	VecDuplicate(tempInvGh, &tempInvGhB);
}

HFeti::~HFeti() {
	delete subClusterSystem;
}

void HFeti::test() {
	subClusterSystem->testSomething();
}

void HFeti::applyPC(Vec g, Vec z) {

	//projectGOrth(g);

	MatMult(BT, g, temp);
	//if (systemNullSpace != PETSC_NULL) MatNullSpaceRemove(systemNullSpace, temp, PETSC_NULL);
	applyPrimalMult(temp, temp);
	//if (systemNullSpace != PETSC_NULL) MatNullSpaceRemove(systemNullSpace, temp, PETSC_NULL);
	MatMult(B, temp, z);

	//projectGOrth(z);

}

void HFeti::applyPrimalMult(Vec in, Vec out) {

	VecCopy(in, tempInv);

	MatMult(A, tempInvGh, tempInvGhB);
	VecCopy(tempInvGhB, tempInvGh);

	VecCopy(tempInv, out);

}

void HFeti::applyInvA(Vec in, IterationManager *itManager) {

	VecCopy(in, globTemp);
	VecCopy(globTempGh, clustTempGh);

	MatNullSpaceRemove(clusterNS, clustTemp, PETSC_NULL);

	//subClusterSystem->setRequiredPrecision(1e-2);

	//PetscViewer v;
	//	if (cluster->clusterColor == 0) {
	//	PetscViewerBinaryOpen(cluster->clusterComm, "../matlab/testH.m", FILE_MODE_WRITE, &v);
	//	VecView(clustTemp, v);
	//
	//	subClusterSystem->setIsVerbose(true);
	//}

	subClusterSystem->solve(clustTemp);
	subClusterSystem->copySolution(clustTemp);

	//if (cluster->clusterColor == 0) {
	//	subClusterSystem->dumpSolution(v);
	//	PetscViewerDestroy(v);
	//}

	if (itManager != NULL) {
		itManager->setIterationData("outFETIit", subClusterSystem->getOutIterations());
		itManager->setIterationData("inFETIit", subClusterSystem->getInIterations());
	}
	inIterations += subClusterSystem->getOutIterations();

	VecCopy(clustTempGh, globTempGh);
	VecCopy(globTemp, in);
}

ASolver* HFeti::instanceOuterSolver(Vec d, Vec lmb) {
	outerPrec = 1e-3;
	lastNorm = 1e-4;
	inCounter = 0;

	ASolver *sol = new CGSolver(this, this, cMan->getPrimal(), 15);
	sol->setProjector(this);

	return sol;
}

void HFeti::setRequiredPrecision(PetscReal reqPrecision) {
	outerPrec = reqPrecision;
}
