#include "ffeti.h"

#include "Utils/logger.h"

FFeti::FFeti(PDCommManager *comMan, Mat A, Vec b, Mat BT, Mat B, Vec lmb,
		NullSpaceInfo *nullSpace, PetscInt localNodeCount, PetscInt fNodesCount,
		PetscInt *fNodes, CoarseProblemMethod cpM, SystemR *sR) :
		Feti1(comMan, A, b, BT, B, lmb, nullSpace, localNodeCount, fNodesCount, fNodes, cpM, sR) {
	{

		PetscInt BRowCount, BLocalColCount, BLocalRowCount;
		PetscInt myStart, myEnd;

		MatGetLocalSize(B, &BLocalRowCount, &BLocalColCount);
		MatGetSize(B, &BRowCount, PETSC_NULL);
		MatGetOwnershipRange(B, &myStart, &myEnd);

		//Vec unitVector;
		Vec bRow;

		VecCreateMPI(comMan->getPrimal(), BLocalColCount, PETSC_DECIDE, &bRow);
		VecCreateMPI(comMan->getPrimal(), BLocalRowCount, PETSC_DECIDE, &fGlobal);

		VecScatterCreateToZero(fGlobal, &fToRoot, &fLocal);

		if (comMan->isPrimalRoot()) {
			MatCreateSeqDense(PETSC_COMM_SELF, BRowCount, BRowCount, PETSC_NULL, &F);
			//MatCreateSeqAIJ(PETSC_COMM_SELF, BRowCount, BRowCount, BRowCount, PETSC_NULL, &F);
		}

		PetscInt ncols;
		const PetscInt *cols;
		const PetscScalar *vals;

		for (int row = 0; row < BRowCount; row++) {
			{

				//VecSet(unitVector, 0);
				//if (comMan->isPrimalRoot()) VecSetValue(unitVector, row, 1, INSERT_VALUES);

				//VecAssemblyBegin(unitVector);
				//VecAssemblyEnd(unitVector);

				//applyMult(unitVector, fGlobal, NULL);

				{

					VecSet(bRow, 0);
					if (row >= myStart && row < myEnd) {
						MatGetRow(B, row, &ncols, &cols, &vals);
						for (int i = 0; i < ncols; i++)
							VecSetValue(bRow, cols[i], vals[i], INSERT_VALUES);
						MatRestoreRow(B, row, &ncols, &cols, &vals);
					}
					VecAssemblyBegin(bRow);
					VecAssemblyEnd(bRow);
				}

				{

					applyInvA(bRow, NULL);
				}
				//if (row == 70) {
				//	PetscViewer v;
				////	PetscViewerBinaryOpen(comMan->getPrimal(), "../matlab/data/fTest.m", FILE_MODE_WRITE, &v);
				//	VecView(bRow, v);
				//	PetscViewerDestroy(v);
				//	}

				MatMult(B, bRow, fGlobal);

				VecScatterBegin(fToRoot, fGlobal, fLocal, INSERT_VALUES, SCATTER_FORWARD);
				VecScatterEnd(fToRoot, fGlobal, fLocal, INSERT_VALUES, SCATTER_FORWARD);

				{

					if (cMan->isPrimalRoot()) {
						PetscScalar *fVals;
						VecGetArray(fLocal, &fVals);

						for (int i = 0; i < BRowCount; i++) {
							if (fVals[i] != 0) {
								MatSetValue(F, i, row, fVals[i], INSERT_VALUES);
							}
						}
						VecRestoreArray(fLocal, &fVals);
						MatAssemblyBegin(F, MAT_FINAL_ASSEMBLY);
						MatAssemblyEnd(F, MAT_FINAL_ASSEMBLY);
					}
				}
			}
		}

		//
		// Prepare G regularization (if necessary)
		//

		Mat RgLOC; // Local matrix of null space of G (tricky, isn't it ;-))
		Mat RRt;

		if (sR != NULL && sR->rDim > 0) {
			VecScatter rScat;
			Vec rLocal;
			VecScatterCreateToZero(sR->systemGNullSpace[0], &rScat, &rLocal);

			PetscInt rRows;
			if (cMan->isPrimalRoot()) {
				VecGetSize(rLocal, &rRows);
				MatCreateSeqDense(PETSC_COMM_SELF, rRows, sR->rDim, PETSC_NULL, &RgLOC);
			}

			for (int i = 0; i < sR->rDim; i++) {
				VecScatterBegin(rScat, sR->systemGNullSpace[i], rLocal, INSERT_VALUES, SCATTER_FORWARD);
				VecScatterEnd(rScat, sR->systemGNullSpace[i], rLocal, INSERT_VALUES, SCATTER_FORWARD);

				if (cMan->isPrimalRoot()) {
					PetscScalar *rVals;
					VecGetArray(rLocal, &rVals);
					for (int j = 0; j < rRows; j++) {
						MatSetValue(RgLOC, j, i, rVals[j], INSERT_VALUES);
					}
					VecRestoreArray(rLocal, &rVals);
					MatAssemblyBegin(RgLOC, MAT_FINAL_ASSEMBLY);
					MatAssemblyEnd(RgLOC, MAT_FINAL_ASSEMBLY);
				}
			}

			if (cMan->isPrimalRoot()) {
				Mat Rt;
				MatTranspose(RgLOC, MAT_INITIAL_MATRIX, &Rt);
				MatMatMult(RgLOC, Rt, MAT_INITIAL_MATRIX, 1, &RRt);
				MatDestroy(Rt);
			}
		}

		//VecDestroy(bRow);
		//Mat Groot;
		//gatherMatrix(G, Groot, 0, cMan->getPrimal());

		if (cMan->isPrimalRoot()) {
			/*
			 PetscViewer v;
			 PetscViewerBinaryOpen(PETSC_COMM_SELF, "../matlab/data/F.m", FILE_MODE_WRITE, &v);
			 MatView(F, v);
			 MatView(Groot, v);
			 PetscViewerDestroy(v);
			 */
			PC pcF;
			PCCreate(PETSC_COMM_SELF, &pcF);
			PCSetOperators(pcF, F, F, SAME_PRECONDITIONER);
			PCSetType(pcF, "cholesky");
			PCSetUp(pcF);

			KSPCreate(PETSC_COMM_SELF, &kspF);
			KSPSetTolerances(kspF, 1e-10, 1e-10, 1e7, 1);
			KSPSetPC(kspF, pcF);
			KSPSetOperators(kspF, F, F, SAME_PRECONDITIONER);

			//MatDestroy(F);
			PCDestroy(pcF);

			Vec gCol, sCol;
			Mat GrootT, S;
			PetscInt gRows, gCols;
			MatGetSize(Groot, &gRows, &gCols);

			VecCreateSeq(PETSC_COMM_SELF, gRows, &gCol);
			VecCreateSeq(PETSC_COMM_SELF, gCols, &sCol);
			MatCreateSeqDense(PETSC_COMM_SELF, gCols, gCols, PETSC_NULL, &S);

			MatTranspose(Groot, MAT_INITIAL_MATRIX, &GrootT);

			PetscInt ncols;
			const PetscInt *cols;
			const PetscScalar *vals;

			for (int col = 0; col < gCols; col++) {
				VecSet(gCol, 0);
				MatGetRow(GrootT, col, &ncols, &cols, &vals);
				for (int i = 0; i < ncols; i++)
					VecSetValue(gCol, cols[i], vals[i], INSERT_VALUES);
				MatRestoreRow(GrootT, col, &ncols, &cols, &vals);
				VecAssemblyBegin(gCol);
				VecAssemblyEnd(gCol);

				KSPSolve(kspF, gCol, gCol);

				MatMult(GrootT, gCol, sCol);

				PetscScalar *sVals;
				VecGetArray(sCol, &sVals);

				for (int i = 0; i < gCols; i++) {
					if (sVals[i] != 0) {
						MatSetValue(S, col, i, sVals[i], INSERT_VALUES);
					}
				}
				VecRestoreArray(sCol, &sVals);
				MatAssemblyBegin(S, MAT_FINAL_ASSEMBLY);
				MatAssemblyEnd(S, MAT_FINAL_ASSEMBLY);

			}

			if (sR != NULL && sR->rDim > 0) {
				MatAXPY(S, 1, RRt, SAME_NONZERO_PATTERN);
			}

			PC pcS;
			PCCreate(PETSC_COMM_SELF, &pcS);
			PCSetOperators(pcS, S, S, SAME_PRECONDITIONER);
			PCSetType(pcS, "cholesky");
			PCSetUp(pcS);

			KSPCreate(PETSC_COMM_SELF, &kspS);
			KSPSetTolerances(kspS, 1e-10, 1e-10, 1e7, 1);
			KSPSetPC(kspS, pcS);
			KSPSetOperators(kspS, S, S, SAME_PRECONDITIONER);

			//MatDestroy(F);
			PCDestroy(pcS);

		}
		//PetscViewer v;
		//PetscViewerBinaryOpen(comMan->getPrimal(), "../matlab/F.m", FILE_MODE_WRITE, &v);
		//MatView(F, v);
		//PetscViewerDestroy(v);
	}
}

void FFeti::applyMult(Vec in, Vec out, IterationManager *info) {

	outIterations++;

	if (cMan->isPrimalRoot()) MyLogger::Instance()->getTimer("BA+BT")->startTimer();

	VecScatterBegin(fToRoot, in, fLocal, INSERT_VALUES, SCATTER_FORWARD);
	VecScatterEnd(fToRoot, in, fLocal, INSERT_VALUES, SCATTER_FORWARD);

	if (cMan->isPrimalRoot()) MatMult(F, fLocal, dBLoc);

	VecScatterBegin(fToRoot, dBLoc, out, INSERT_VALUES, SCATTER_REVERSE);
	VecScatterEnd(fToRoot, dBLoc, out, INSERT_VALUES, SCATTER_REVERSE);

	if (cMan->isPrimalRoot()) MyLogger::Instance()->getTimer("BA+BT")->stopTimer();

}

void FFeti::solve() {
	//Feti1::solve();

	Vec d;
	VecDuplicate(lmb, &d);

	VecCopy(b, temp);

	applyInvA(temp, NULL);
	MatMult(B, temp, d);

	MatMultTranspose(R, b, e);

	Vec eLOC, dLOC, alpha;

	VecDuplicate(e, &alpha);

	VecScatter dualScatter, modesScatter;
	VecScatterCreateToZero(d, &dualScatter, &dLOC);
	VecScatterCreateToZero(e, &modesScatter, &eLOC);

	VecScatterBegin(dualScatter, d, dLOC, INSERT_VALUES, SCATTER_FORWARD);
	VecScatterEnd(dualScatter, d, dLOC, INSERT_VALUES, SCATTER_FORWARD);

	VecScatterBegin(modesScatter, e, eLOC, INSERT_VALUES, SCATTER_FORWARD);
	VecScatterEnd(modesScatter, e, eLOC, INSERT_VALUES, SCATTER_FORWARD);
	if (cMan->isPrimalRoot()) {

		// Alpha computation upfront
		Vec Fpd, GFpd, alphaLOC, lmbLOC, Galph;
		VecDuplicate(dLOC, &Fpd);
		VecDuplicate(dLOC, &lmbLOC);
		VecDuplicate(dLOC, &Galph);
		VecCopy(dLOC, lmbLOC);

		VecDuplicate(eLOC, &GFpd);
		VecDuplicate(eLOC, &alphaLOC);

		KSPSolve(kspF, dLOC, Fpd);
		MatMultTranspose(Groot, Fpd, GFpd);

		VecAXPY(GFpd, -1, eLOC);
		KSPSolve(kspS, GFpd, eLOC);

		MatMult(Groot, eLOC, Galph);
		VecAXPY(lmbLOC, -1, Galph);

		KSPSolve(kspF, lmbLOC, dLOC);

	}

	VecScatterBegin(dualScatter, dLOC, lmb, INSERT_VALUES, SCATTER_REVERSE);
	VecScatterEnd(dualScatter, dLOC, lmb, INSERT_VALUES, SCATTER_REVERSE);

	VecScatterBegin(modesScatter, eLOC, alpha, INSERT_VALUES, SCATTER_REVERSE);
	VecScatterEnd(modesScatter, eLOC, alpha, INSERT_VALUES, SCATTER_REVERSE);

	VecScale(lmb, -1);
	MatMultAdd(BT, lmb, b, u);
	VecScale(lmb, -1);
	applyInvA(u, NULL);
	VecScale(alpha, -1);
	MatMultAdd(R, alpha, u, u);

	PetscReal normB, error;

	if (isVerbose) {

		VecNorm(b, NORM_2, &normB);

		applyPrimalMult(u, temp);
		VecAXPY(temp, -1, b);
		MatMult(BT, lmb, tempInv);
		VecAXPY(temp, 1, tempInv);

		VecNorm(temp, NORM_2, &error);

		PetscPrintf(cMan->getPrimal(), "Relative error: %e\n\n", error / normB);

	}
	if (isVerbose) {
		PetscReal feasErr, uNorm;

		MatMult(B, u, d);

		VecNorm(d, NORM_2, &feasErr);
		VecNorm(u, NORM_2, &uNorm);

		PetscPrintf(cMan->getPrimal(), "Feasibility err: %e \n", feasErr / uNorm);
	}
}
