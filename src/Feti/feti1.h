#ifndef JAPET_FETI_FETI1_H
#define JAPET_FETI_FETI1_H

#include "Feti/afeti.h"

/**
 @brief	Rather general (and functional) implementation of FETI-1 algorithm. Tearing of global
 domain and distribution of subdomains is done in preprocesing of Mesh object.

 Currently, CG implementation in solver.h is used for outer cycle and Petsc direct solver
 for inner cycle.

 **/

class Feti1: public AFeti {
protected:
	Mat A; ///< Global stifles matrix
	Mat Aloc; ///< Local part of stiffness matrix
	KSP kspA; ///< Local A solver

	MatNullSpace locNS; ///< local null space of local A

	Vec tempInv, tempInvGh, tempInvGhB;

	PetscLogStage aFactorStage;
public:
	Feti1(PDCommManager *comMan, Mat A, Vec b, Mat BT, Mat B, Vec lmb,
			NullSpaceInfo *nullSpace, PetscInt localNodeCount, PetscInt fNodesCount,
			PetscInt *fNodes, CoarseProblemMethod cpM = ParaCG,
			SystemR *sR = PETSC_NULL);
	~Feti1();
	virtual void applyInvA(Vec in, IterationManager *itManager);
	virtual void applyPC(Vec g, Vec z);

	virtual void solve();

	virtual void applyPrimalMult(Vec in, Vec out);
};

#endif