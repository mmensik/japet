#include "cgsolver.h"

#include "Utils/logger.h"

CGSolver::~CGSolver() {

}

void CGSolver::solve(Vec b, Vec x) {

	itManager.reset();
	itManager.setTitle("CG");

	Vec g, z, p;
	VecDuplicate(b, &g);
	VecDuplicate(b, &z);
	VecDuplicate(b, &p);

	VecNorm(b, NORM_2, &bNorm);

	Vec temp;
	VecDuplicate(b, &temp);

	VecCopy(b, g);

	sApp->setRequiredPrecision(MAXPREC);
	sApp->applyMult(x, temp, &itManager);
	VecAYPX(g, -1, temp); // g = Ax - b

	sProj->applyProjection(g, g);
	sPC->applyPC(g, z);
	sProj->applyProjection(z, z);

	VecNorm(g, NORM_2, &rNorm);
	r0Norm = rNorm;

	VecCopy(z, p);

	if (bNorm > 1e-16) {
		PetscReal relativeCoef = MAX(bNorm, r0Norm);
		PetscReal gTzPrev, gTz;

		//solProj->applyProjection(g);
		//	sPC->applyPC(g, z);
		VecDot(g, z, &gTz);

		while (!sCtr->isConverged(getItCount(), rNorm, relativeCoef, &g)) {

			MyLogger::Instance()->getTimer("Iteration")->startTimer();
			itManager.setIterationData("Rel. err", rNorm / relativeCoef);
			nextIteration();

			PetscScalar pAp;
			sApp->applyMult(p, temp, &itManager);

			PetscReal tNorm;
			VecNorm(temp, NORM_2, &tNorm);

			VecDot(p, temp, &pAp);

			PetscReal a = gTz / pAp;

			if (tNorm < 1e-16) {
				//	PetscPrintf(PETSC_COMM_SELF, "BREAK \n");
				break;
			}

			VecAXPY(x, -a, p);

			if (restartRate != -1 && getItCount() > 1
					&& getItCount() % restartRate == 0) {

				VecCopy(b, g);
				sApp->applyMult(x, temp, &itManager);
				VecAYPX(g, -1, temp); // g = Ax - b

				sProj->applyProjection(g, g);
				sPC->applyPC(g, z);
				sProj->applyProjection(z, z);

				VecCopy(z, p);
				VecDot(g, z, &gTz);
			} else {

				VecAXPY(g, -a, temp);

				sProj->applyProjection(g, g);
				sPC->applyPC(g, z);
				sProj->applyProjection(z, z);

				gTzPrev = gTz;

				VecDot(g, z, &gTz);

				PetscReal beta = gTz / gTzPrev;
				VecAYPX(p, beta, z);

			}
			VecNorm(g, NORM_2, &rNorm);

			if (gTz != gTz) {
				PetscPrintf(PETSC_COMM_WORLD, "ERROR!!! %a \n", rNorm);
				break;
			}
			MyLogger::Instance()->getTimer("Iteration")->stopTimer();
		}
	} else {
		VecSet(x, 0);
	}

	VecDestroy(g);
	VecDestroy(z);
	VecDestroy(temp);
	VecDestroy(p);
}
