#ifndef JAPET_SOLVERS_ASOLVER_H
#define JAPET_SOLVERS_ASOLVER_H

#include "Utils/configManager.h"
#include "Utils/iterationManager.h"
#include "Utils/timer.h"

class SolverApp {
public:
	virtual void applyMult(Vec in, Vec out, IterationManager* info = NULL) = 0;
	virtual void setRequiredPrecision(PetscReal reqPrecision);
};

class SolverCtr {
public:
	virtual bool isConverged(PetscInt itNum, PetscReal rNorm, PetscReal bNorm,
			Vec *vec) = 0;
};

class SolverPreconditioner {
public:
	virtual void applyPC(Vec g, Vec z) = 0;
};

class SolverProjector {
public:
	virtual void applyProjection(Vec v, Vec vp) = 0;
};

class SolverInvertor {
public:
	virtual void applyInversion(Vec b, Vec x) = 0;
};

class ASolver {
protected:
	SolverCtr *sCtr;
	SolverProjector *sProj;
	SolverPreconditioner *sPC;

public:
	ASolver() {};

	virtual ~ASolver() {};

	virtual void solve(Vec b, Vec x) = 0;

	virtual void setIsVerbose(bool verbose) {

	}

	virtual void saveIterationInfo(const char *filename, bool rewrite = true) {
		//Has no meaning for factorization
	}

	void setSolverCtr(SolverCtr *sc) {
		sCtr = sc;
	}

	void setPreconditioner(SolverPreconditioner *sp) {
		sPC = sp;
	}

	void setProjector(SolverProjector *sp) {
		sProj = sp;
	}
};

#endif