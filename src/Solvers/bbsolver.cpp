#include "bbsolver.h"

void BBSolver::projectedMult(Vec in, Vec out) {
	if (sProj != NULL) sProj->applyProjection(in, in);
	sApp->applyMult(in, out, &itManager);
	if (sProj != NULL) sProj->applyProjection(out, out);
}

void BBSolver::solve(Vec b, Vec x) {

	itManager.reset();
	itManager.setTitle("BB");

	double precision = 1e-6;

	sApp->setRequiredPrecision(precision);

	VecNorm(b, NORM_2, &bNorm);

	if (bNorm > 1e-16) {

		Vec g, z, temp;
		VecDuplicate(b, &g);
		VecDuplicate(b, &z);
		VecDuplicate(b, &temp);

		VecCopy(b, g);
		projectedMult(x, temp);
		VecAYPX(g, -1, temp); // g = Ax - b

		VecNorm(g, NORM_2, &rNorm);
		r0Norm = rNorm;

		PetscReal relativeCoef = MAX(bNorm, r0Norm);

		PetscReal tempTg, gTg;
		PetscReal alpha;

		projectedMult(g, temp);

		VecDot(g, g, &gTg);
		VecDot(temp, g, &tempTg);

		alpha = gTg / tempTg;

		VecAXPY(x, -alpha, g);

		VecCopy(b, g);
		projectedMult(x, temp);
		VecAYPX(g, -1, temp); // g = Ax - b

		VecNorm(g, NORM_2, &rNorm);

		while (!sCtr->isConverged(getItCount(), rNorm, relativeCoef, &g)) {

			itManager.setIterationData("Rel. err", rNorm / relativeCoef);
			itManager.setIterationData("alpha", alpha);
			itManager.setIterationData("prec", precision);
			nextIteration();

			//alpha_old = alpha;

			VecAXPY(x, -alpha, g);

			if (precision > rNorm) {
				precision = rNorm;
				sApp->setRequiredPrecision(precision);
			} else {
				precision = precision * 0.7;
				sApp->setRequiredPrecision(precision);
			}

			if (isDueRestart(getItCount())) {
				sApp->setRequiredPrecision(precision);
			} else { // Application only for new alpha
				sApp->setRequiredPrecision(precision * aUnprecision);
			}
			projectedMult(g, temp);

			VecDot(g, g, &gTg);
			VecDot(temp, g, &tempTg);

			PetscReal alphaOld = alpha;
			alpha = gTg / tempTg;

			if (isDueRestart(getItCount())) {
				VecCopy(b, g);
				sApp->setRequiredPrecision(precision);
				projectedMult(x, temp);
				VecAYPX(g, -1, temp); // g = Ax - b
			} else {
				VecAXPY(g, -alphaOld, temp);
			}

			VecNorm(g, NORM_2, &rNorm);
		}

	} else {
		VecSet(x, 0);
	}
}

bool BBSolver::isDueRestart(int itNumber) {
	return itNumber % restartRate == 0;
}