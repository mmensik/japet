#ifndef JAPET_SOLVERS_ASINSOLVER_H
#define JAPET_SOLVERS_ASINSOLVER_H

#include "Solvers/solver.h"

const double PI = 4.0 * std::atan(1.0);

class ASinSolver: public Solver {

	void projectedMult(Vec in, Vec out);

	PetscReal tau;
	PetscReal phi;

	PetscInt restartRate;

	bool isDueRestart(int itNumber);

public:
	ASinSolver(Mat A) :
			Solver(A) {
		tau = ConfigManager::Instance()->asinTau;
		phi = (sqrt(5) - 1) / 2;
		restartRate = ConfigManager::Instance()->asinRestart;
	}

	ASinSolver(SolverApp *sa, SolverPreconditioner *pc = NULL, MPI_Comm comm =
			MPI_COMM_WORLD) :
			Solver(sa, pc, comm) {
		tau = ConfigManager::Instance()->asinTau;
		phi = (sqrt(5) - 1) / 2;
		restartRate = ConfigManager::Instance()->asinRestart;
	}

	void solve(Vec b, Vec x); ///< begin solving
};

#endif