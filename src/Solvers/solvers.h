#ifndef JAPET_SOLVERS_H
#define JAPET_SOLVERS_H

#include "Solvers/cgsolver.h"
#include "Solvers/asinsolver.h"
#include "Solvers/steepestdescent.h"
#include "Solvers/bbsolver.h"

#endif