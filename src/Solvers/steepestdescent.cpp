#include "steepestdescent.h"


void SteepestDescent::projectedMult(Vec in, Vec out) {
	if (sProj != NULL) sProj->applyProjection(in, in);
	sApp->applyMult(in, out, &itManager);
	if (sProj != NULL) sProj->applyProjection(out, out);
}

void SteepestDescent::solve(Vec b, Vec x) {

	itManager.reset();
	itManager.setTitle("SteepestDescent Solver");

	double precision = 1e-6;
	sApp->setRequiredPrecision(precision);

	VecNorm(b, NORM_2, &bNorm);

	if (bNorm > 1e-16) { // right hand side is too small for computations

		Vec g, p, temp;

		VecDuplicate(b, &g);
		VecDuplicate(b, &p);
		VecDuplicate(b, &temp);

		VecCopy(b, g);
		projectedMult(x, temp);
		VecAYPX(g, -1, temp); // g = Ax - b

		VecNorm(g, NORM_2, &rNorm);
		r0Norm = rNorm;

		PetscReal relativeCoef = MAX(bNorm, r0Norm);

		PetscReal pTg, gTg;
		PetscReal alpha;

		projectedMult(g, p);

		while (!sCtr->isConverged(getItCount(), rNorm, relativeCoef, &g)) {

			itManager.setIterationData("Rel. err", rNorm / relativeCoef);
			itManager.setIterationData("alpha", alpha);

			nextIteration();

			VecDot(g, g, &gTg);
			VecDot(p, g, &pTg);

			alpha = gTg / pTg;

			VecAXPY(x, -alpha, g);

			VecCopy(b, g);
			projectedMult(x, temp);
			VecAYPX(g, -1, temp); // g = Ax - b

			VecNorm(g, NORM_2, &rNorm);

			if (precision > rNorm) {
				precision = rNorm;
				sApp->setRequiredPrecision(precision);
			} else {
				precision = precision * 0.8;
				sApp->setRequiredPrecision(precision);
			}

			projectedMult(g, p);
		}

	} else {
		VecSet(x, 0);
	}
}
