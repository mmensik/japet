#include "solver.h"

void SolverApp::setRequiredPrecision(PetscReal reqPrecision) {

}

Solver::Solver(Mat A, SolverPreconditioner *PC, MPI_Comm comm) {

	this->A = A;
	this->comm = comm;

	sApp = this;
	this->sPC = PC;

	this->init();
}

Solver::Solver(SolverApp *sa, SolverPreconditioner *PC, MPI_Comm comm) {
	this->sApp = sa;
	this->comm = comm;
	this->sPC = PC;

	this->init();
}

Solver::~Solver() {

}

void Solver::init() {

	itManager.setComm(comm);

	sCtr = this;
	precision = 1e-3;
}

void Solver::applyMult(Vec in, Vec out, IterationManager *info) {
	MatMult(A, in, out);
}

void Solver::applyPC(Vec r, Vec rz) {
	VecCopy(r, rz);
}

bool Solver::isConverged(PetscInt itNumber, PetscReal rNorm, PetscReal bNorm,
		Vec *x) {
	return rNorm / bNorm < precision;
}

void Solver::nextIteration() {
	itManager.setIterationData("!normG", rNorm);
	itManager.setIterationData("R", pow(rNorm / r0Norm, 2.0
			/ (double) itManager.getItCount()));
	itManager.nextIteration();
}