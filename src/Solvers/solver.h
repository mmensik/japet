/**	@file		solver.h
 @author Martin Mensik
 @date 	2010
 @brief	File containing solvers
 */

#ifndef SOLVER_H
#define SOLVER_H

#include <cmath>
#include <vector>

#include "petscmat.h"
#include "petscksp.h"

#include "Solvers/asolver.h" 

const double MAXPREC = 1e-16;

enum StepType {
	CG, Expansion, Proportion
};

class FinitSolverStub: public ASolver {

	SolverInvertor *invertor;
public:

	FinitSolverStub(SolverInvertor *inv) {
		this->invertor = inv;
	}

	virtual void solve(Vec b, Vec x) {
		invertor->applyInversion(b, x);
	}

};

class Solver: public ASolver,
		public SolverApp,
		public SolverCtr,
		public SolverPreconditioner {
private:
	Mat A; ///< Matrix A in problem A*x = b
	PetscReal precision;

protected:

	SolverApp *sApp;

	MPI_Comm comm;

	PetscReal rNorm, r0Norm, bNorm;

	void nextIteration();
	void init();

public:
	IterationManager itManager;

	Solver(Mat A, SolverPreconditioner *PC = NULL,
			MPI_Comm comm = MPI_COMM_WORLD);
	Solver(SolverApp *sa, SolverPreconditioner *PC = NULL, MPI_Comm comm =
			MPI_COMM_WORLD);
	virtual ~Solver();

	void setIterationData(std::string name, PetscReal value) {
		itManager.setIterationData(name, value);
	}
	void setIsVerbose(bool isVerbose) {
		itManager.setIsVerbose(isVerbose);
	}

	int getItCount() {
		return itManager.getItCount();
	}

	void applyMult(Vec in, Vec out, IterationManager *info = NULL);
	void applyPC(Vec r, Vec z);
	bool isConverged(PetscInt itNum, PetscReal rNorm, PetscReal bNorm, Vec *vec);

	void setSolverApp(SolverApp *sa) {
		sApp = sa;
	}

	void setPrecision(PetscReal precision) {
		this->precision = precision;
	}
	void saveIterationInfo(const char *filename, bool rewrite = true) {
		itManager.saveIterationInfo(filename, rewrite);
	}
	void setTitle(std::string title) {
		itManager.setTitle(title);
	}

	virtual void solve(Vec b, Vec x) = 0;
};

#endif
