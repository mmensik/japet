#ifndef JAPET_SOLVERS_CGSOLVER_H
#define JAPET_SOLVERS_CGSOLVER_H

#include "Solvers/solver.h"

class CGSolver: public Solver {

	int restartRate;

	void initSolver();
public:
	CGSolver(Mat A) :
			Solver(A) {
	}

	CGSolver(SolverApp *sa, SolverPreconditioner *pc = NULL, MPI_Comm comm =
			MPI_COMM_WORLD, int restartRate = -1) :
			Solver(sa, pc, comm) {
		this->restartRate = restartRate;
	}

	~CGSolver();

	void solve(Vec b, Vec x); ///< begin solving
};

#endif