#ifndef JAPET_SOLVERS_BBSOLVER_H
#define JAPET_SOLVERS_BBSOLVER_H

#include "Solvers/solver.h"
#include "Utils/configManager.h"

class BBSolver: public Solver {

	void projectedMult(Vec in, Vec out);
	bool isDueRestart(int itNumber);

	PetscInt restartRate;
	PetscReal aUnprecision;
public:
	BBSolver(Mat A) :
			Solver(A) {
		restartRate = ConfigManager::Instance()->asinRestart;
	}

	BBSolver(SolverApp *sa, SolverPreconditioner *pc = NULL, MPI_Comm comm =
			MPI_COMM_WORLD) :
			Solver(sa, pc, comm) {
		restartRate = ConfigManager::Instance()->asinRestart;
		aUnprecision = ConfigManager::Instance()->bbAUnprecision;
	}

	void solve(Vec b, Vec x); ///< begin solving

};

#endif