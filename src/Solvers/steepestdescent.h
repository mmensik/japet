#ifndef JAPET_SOLVERS_STEEPESTDESCENT_H
#define JAPET_SOLVERS_STEEPESTDESCENT_H

#include "Solvers/solver.h"

class SteepestDescent: public Solver {

	void projectedMult(Vec in, Vec out);

public:
	SteepestDescent(Mat A) :
			Solver(A) {
	}

	SteepestDescent(SolverApp *sa, SolverPreconditioner *pc = NULL,
			MPI_Comm comm = MPI_COMM_WORLD) :
			Solver(sa, pc, comm) {
	}

	void solve(Vec b, Vec x); ///< begin solving
};

#endif