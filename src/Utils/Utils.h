/*

 */
#ifndef MESGERR
#define MESGERR 1
#endif

#ifndef JAPETUTILS_H_
#define JAPETUTILS_H_

#include "petscsys.h"
#include "petscmat.h"

#include "Utils/Datatypes.h"

//*
// Gather matrix on one (root) proces
//

void gatherMatrix(Mat A, Mat Aloc, int root, MPI_Comm comm);

#endif /* JAPETUTILS_H_ */
