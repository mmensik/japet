#include "pdcommmanager.h"

PDCommManager::PDCommManager(MPI_Comm parent, PDStrategy strategy) {
	parentComm = parent;

	MPI_Comm_size(parentComm, &parSize);
	MPI_Comm_rank(parentComm, &parRank);

	switch (strategy) {
	case ALL_ALL_SAMEROOT:
		MPI_Comm_dup(parentComm, &primalComm);
		MPI_Comm_dup(parentComm, &dualComm);
		break;
	case ALL_ONE_SAMEROOT:
		MPI_Comm_dup(parentComm, &primalComm);
		MPI_Comm_split(parentComm, (parRank == 0) ? 0 : MPI_UNDEFINED, 0, &dualComm);
		break;
	case ALL_TWO_SAMEROOT:
		MPI_Comm_dup(parentComm, &primalComm);
		MPI_Comm_split(parentComm, (parRank < 2) ? 0 : MPI_UNDEFINED, 0, &dualComm);
		break;
	case HECTOR:
		MPI_Comm_dup(parentComm, &primalComm);
		MPI_Comm_split(parentComm, (parRank < 24) ? 0 : MPI_UNDEFINED, 0, &dualComm);
		break;
	case TEST:
		MPI_Comm_split(parentComm, (parRank % 3 == 0) ? 0 : MPI_UNDEFINED, 0, &primalComm);
		MPI_Comm_split(parentComm, (parRank % 2 == 0) ? 0 : MPI_UNDEFINED, 0, &dualComm);
		break;
	case SAME_COMMS:
		primalComm = parentComm;
		dualComm = parentComm;
		break;
	case LAST_ROOT:
		MPI_Comm_split(parentComm, parRank > 0 ? 0 : MPI_UNDEFINED, -parRank, &primalComm);
		dualComm = primalComm;
	}

	if (isPrimal()) {
		MPI_Comm_rank(primalComm, &pRank);
		MPI_Comm_size(primalComm, &pSize);
	} else {
		pRank = -1;
		pSize = -1;
	}

	if (isDual()) {
		MPI_Comm_rank(dualComm, &dRank);
		MPI_Comm_size(dualComm, &dSize);
	} else {
		dRank = -1;
		dSize = -1;
	}

}

void PDCommManager::printSummary() {
	if (isPrimal()) PetscPrintf(PETSC_COMM_SELF, "[%d] is PRIMAL with rank %d \n", parRank, pRank);
	if (isDual()) PetscPrintf(PETSC_COMM_SELF, "[%d] is DUAL with rank %d \n", parRank, dRank);
}
