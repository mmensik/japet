#include "logger.h"


MyLogger* MyLogger::instance = NULL;

MyLogger* MyLogger::Instance() {
	if (!instance) {
		instance = new MyLogger();
	}
	return instance;
}