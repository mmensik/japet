#ifndef JAPET_UTILS_ITERATIONMANAGER_H_
#define JAPET_UTILS_ITERATIONMANAGER_H_

#include <vector>
#include <map>
#include <string>

#include "petscsys.h"

struct IterationInfo {
	int itNumber;
	PetscReal rNorm;
	std::vector<PetscReal> itData;
};

class IterationManager {
	std::string title;
	MPI_Comm comm;

	int itCounter;
	std::vector<IterationInfo> itInfo;
	std::map<std::string, PetscReal> iterationData;

	bool isVerbose;
public:
	IterationManager() {
		itCounter = 0;
		isVerbose = false;
		this->comm = MPI_COMM_SELF;
	}
	void setTitle(std::string title) {
		this->title = title;
	}
	void setComm(MPI_Comm comm) {
		this->comm = comm;
	}

	void nextIteration();
	void setIterationData(std::string name, PetscReal value) {
		iterationData[name] = value;
	}
	void saveIterationInfo(const char *filename, bool rewrite = true);

	void setIsVerbose(bool isVerbose) {
		this->isVerbose = isVerbose;
	}
	int getItCount() {
		return itCounter;
	}
	void reset() {
		itCounter = 0;
		itInfo.clear();
	}

};


#endif