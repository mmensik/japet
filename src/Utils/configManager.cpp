#include "configManager.h"


ConfigManager* ConfigManager::instance = NULL;

ConfigManager* ConfigManager::Instance() {
	if (!instance) {
		instance = new ConfigManager();
	}
	return instance;
}

ConfigManager::ConfigManager() {

	maxIt = 60;

	E = 2.1e5;
	mu = 0.3;
	h = 2.0;
	Hx = 200;
	Hy = 100;
	m = 3;
	n = 3;
	problem = 0;

	clustM = 2;
	clustN = 2;

	reqSize = 100;

	innerSolver = BB;
	asinTau = 1e-4;
	asinRestart = 4;

	bbAUnprecision = 10;

	PetscTruth flg;

	coarseProblemMethod = MasterWork;
	pdStrategy = SAME_COMMS;
	saveOutputs = false;

	char tName[PETSC_MAX_PATH_LEN] = "FetiTest.log";

	char optName[PETSC_MAX_PATH_LEN] = "japet.cfg";

	PetscOptionsGetString(PETSC_NULL, "-japet_config", optName, PETSC_MAX_PATH_LEN -1, &flg);
	PetscOptionsInsertFile(PETSC_COMM_WORLD, optName, PETSC_FALSE);

	PetscOptionsGetInt(PETSC_NULL, "-japet_req_size", &reqSize, PETSC_NULL);
	PetscOptionsGetInt(PETSC_NULL, "-japet_max_it", &maxIt, PETSC_NULL);
	PetscOptionsGetInt(PETSC_NULL, "-japet_problem", &problem, PETSC_NULL);
	PetscOptionsGetInt(PETSC_NULL, "-japet_m", &m, PETSC_NULL);
	PetscOptionsGetInt(PETSC_NULL, "-japet_n", &n, PETSC_NULL);
	PetscOptionsGetInt(PETSC_NULL, "-japet_clust_m", &clustM, PETSC_NULL);
		PetscOptionsGetInt(PETSC_NULL, "-japet_clust_n", &clustN, PETSC_NULL);
	PetscOptionsGetReal(PETSC_NULL, "-japet_h", &h, PETSC_NULL);
	PetscOptionsGetReal(PETSC_NULL, "-japet_size_x", &Hx, PETSC_NULL);
	PetscOptionsGetReal(PETSC_NULL, "-japet_size_y", &Hy, PETSC_NULL);
	PetscOptionsGetString(PETSC_NULL, "-japet_name", tName, PETSC_MAX_PATH_LEN - 1, &flg);

	PetscOptionsGetInt(PETSC_NULL, "-japet_cpmethod", (PetscInt*) &coarseProblemMethod, PETSC_NULL);
	PetscOptionsGetInt(PETSC_NULL, "-japet_pd_strategy", (PetscInt*) &pdStrategy, PETSC_NULL);
	PetscOptionsGetTruth(PETSC_NULL, "-japet_save_output", (PetscTruth*) &saveOutputs, PETSC_NULL);

	PetscOptionsGetReal(PETSC_NULL, "-japet_e", &E, PETSC_NULL);
	PetscOptionsGetReal(PETSC_NULL, "-japet_mu", &mu, PETSC_NULL);

	PetscOptionsGetInt(PETSC_NULL, "-japet_isolver", &innerSolver, PETSC_NULL);
	PetscOptionsGetReal(PETSC_NULL, "-japet_asin_tau", &asinTau, PETSC_NULL);
	PetscOptionsGetInt(PETSC_NULL, "-japet_asin_restart", &asinRestart, PETSC_NULL);

	PetscOptionsGetReal(PETSC_NULL, "-japet_bb_aunprec", &bbAUnprecision, PETSC_NULL);

	name = tName;

}
