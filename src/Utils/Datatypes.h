#ifndef JAPET_UTILS_DATATYPE_H
#define JAPET_UTILS_DATATYPE_H

typedef int idxtype;

enum CoarseProblemMethod {
	ParaCG = 0, MasterWork = 1, ORTO = 2
};

enum InnerSolverType {
	STEEPEST_DESCENT = 0, BB = 1, ASIN = 2
};

enum PDStrategy {
	ALL_ALL_SAMEROOT = 0,
	ALL_ONE_SAMEROOT = 1,
	ALL_TWO_SAMEROOT = 2,
	HECTOR = 3,
	TEST = 4,
	SAME_COMMS = 5,
	LAST_ROOT = 6
};

#endif