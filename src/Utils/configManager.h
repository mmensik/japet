#ifndef JAPET_UTILS_CONFIGMANAGER_H_
#define JAPET_UTILS_CONFIGMANAGER_H_

#include "petscsys.h"

#include "Utils/Datatypes.h"

class ConfigManager {
	static ConfigManager *instance;
	ConfigManager();
public:
	PetscInt maxIt;

	PetscInt reqSize;

	PetscInt m;
	PetscInt n;
	PetscReal h;
	PetscReal Hx;
	PetscReal Hy;

	PetscInt clustM;
	PetscInt clustN;

	PetscInt problem; //< 0 - Laplace, 1 - lin.elasticity
	PetscInt innerSolver;

	PetscInt asinRestart;
	PetscReal asinTau;

	PetscReal bbAUnprecision;

	CoarseProblemMethod coarseProblemMethod;
	PDStrategy pdStrategy;

	char *name;

	PetscReal E;
	PetscReal mu;

	bool saveOutputs;

	static ConfigManager* Instance();
};

#endif