#ifndef JAPET_UTILS_PDCOMMMANAGER_H_
#define JAPET_UTILS_PDCOMMMANAGER_H_

#include "petscsys.h"

#include "Utils/Datatypes.h"

class PDCommManager {
	MPI_Comm parentComm;
	MPI_Comm primalComm;
	MPI_Comm dualComm;

	int parRank, pRank, dRank;
	int parSize, pSize, dSize;

public:
	PDCommManager(MPI_Comm parent, PDStrategy strategy);
	~PDCommManager() {
		if (!isSameComm()) {
			if (isPrimal()) MPI_Comm_free(&primalComm);
			if (isDual()) MPI_Comm_free(&dualComm);
		}
	}

	void printSummary();

	bool isSameComm() {

		bool iSC = false;

		if (isPrimal() && isDual()) {
			int result;
			MPI_Comm_compare(primalComm, dualComm, &result);
			iSC = result == MPI_IDENT;
		}

		return iSC;
	}

	MPI_Comm getPrimal() {
		return primalComm;
	}
	MPI_Comm getDual() {
		return dualComm;
	}
	MPI_Comm getParen() {
		return parentComm;
	}

	bool isPrimal() {
		return primalComm != MPI_COMM_NULL;
	}
	bool isDual() {
		return dualComm != MPI_COMM_NULL;
	}

	bool commonRoots() {
		return true;
	}
	bool isDualRoot() {
		return !dRank;
	}
	bool isPrimalRoot() {
		return !pRank;
	}

	int getPrimalSize() {
		return pSize;
	}
	int getDualSize() {
		return dSize;
	}

	int getPrimalRank() {
		return pRank;
	}
	int getDualRank() {
		return dRank;
	}

	void show() {
		PetscPrintf(PETSC_COMM_SELF, "[%d] ", parRank);
		if (isPrimal()) PetscPrintf(PETSC_COMM_SELF, "\t prim: %d ", pRank);
		if (isDual()) PetscPrintf(PETSC_COMM_SELF, "\t dual: %d ", dRank);
		PetscPrintf(PETSC_COMM_SELF, "\n ");
	}
};

#endif