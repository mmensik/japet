#ifndef JAPET_UTILS_TIMMER_H_
#define JAPET_UTILS_TIMMER_H_

#include <vector>

#include "petscsys.h"

struct MarkedTime {
	const char* title;
	PetscLogDouble time;
};

class MyTimer {
	PetscLogDouble total, start, end;
	PetscInt laps;

	std::vector<MarkedTime*> markedTimes;
public:
	MyTimer() {
		laps = 0;
		total = 0;
	}

	void startTimer() {
		PetscGetTime(&start);
	}
	void stopTimer() {
		PetscGetTime(&end);

		total += end - start;
		laps++;
	}
	void markTime(const char * title);

	PetscLogDouble getAverageTime() {
		return total / laps;
	}
	PetscInt getLapCount() {
		return laps;
	}
	PetscLogDouble getTotalTime() {
		return total;
	}

	PetscLogDouble getAverageOverComm(MPI_Comm comm);

	PetscLogDouble getMaxOverComm(MPI_Comm comm);

	void printMarkedTime(MPI_Comm comm);
};

#endif