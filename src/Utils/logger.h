#ifndef JAPET_UTILS_LOGGER_H_
#define JAPET_UTILS_LOGGER_H_

#include "Utils/timer.h"

class MyLogger {
	static MyLogger *instance;

	MyLogger() {

	}

	std::map<std::string, MyTimer*> timers;
public:
	static MyLogger* Instance();

	MyTimer* getTimer(std::string timer) {

		MyTimer* t = timers[timer];
		if (t == NULL) {
			t = new MyTimer();
			timers[timer] = t;
		}

		return t;
	}
};


#endif